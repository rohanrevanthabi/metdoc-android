import React, { Component } from "react";
import { View, Image, StyleSheet, Text, ImageBackground } from "react-native";
import { Container, Footer ,Toast} from "native-base";
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import TouchID from 'react-native-touch-id';
import moment from 'moment';

class Splash extends Component {
    state = {
        loading: true
      }
    
      async componentDidMount() {
          this.setState({ loading: false })
          this.sessionExists();
    }

    sessionExists = async () => {
        const token = await AsyncStorage.getItem('SessionID');
        const userId = await AsyncStorage.getItem("userId");
        const FingerPrintSuccess = await AsyncStorage.getItem("FingerPrintSuccess");
        const skipedFinger = await AsyncStorage.getItem("FingerPrintSkiped");
        if (token) {
          if(FingerPrintSuccess){
            Actions.dashboard();
            const optionalConfigObject = {
                title: 'FingerPrint Required', // Android
                imageColor: '#038cfc', // Android
                imageErrorColor: '#e86868', // Android
                sensorDescription: 'Touch sensor', // Android
                sensorErrorDescription: 'Failed', // Android
                cancelText: 'Cancel', // Android
                unifiedErrors: true, // use unified error messages (default false)
                fallbackLabel: '',
                passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
              };
              TouchID.authenticate('', optionalConfigObject)
                .then(success => {
                    this.showToast("FingerPrint Authenticated Successfully");
                })
                .catch(error => {
                    console.log(error.message);
                    if(error.message=="User canceled authentication"){
                        this.showToast(error.message);
                         AsyncStorage.setItem('userCanceled',error.message);
                        var userCancel=error.message
                        Actions.login({userCancel});
                      }
                });
          }           
          else if(skipedFinger){
            Actions.login();
        }else{
            // Not Enrole FingerPrint In Your MobilePhone
            Actions.dashboard();
        }
        }
       
         else {
            setTimeout(function () { Actions.login() }, 2000);
        }
    };

    showToast = (message) => {
        Toast.show({
            text: message,
            buttonText: "Okay",
            duration: 3000
        })
      }

    render() {
        if (this.state.loading) {
            return (
              <View></View>
            );
          }
        return (
            <Container>
            
                <View style={styles.container}>
                    <Image
                        source={require('../assets/PathworksLogo.png')}
                        style={{
                            width: 80,
                            height: 80,
                            borderColor: '#FFF',
                            borderWidth: 2,
                            borderRadius: 50,
                        }}
                    />
                </View>

                <Footer style={{ backgroundColor: '#4FABF6' }}>
                    <Text style={{ color: 'white' }}>Met Doc @{moment().year()}</Text>
                </Footer>
            </Container>
        );
    }
}
export default Splash;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4FABF6'
    }
});