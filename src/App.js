import React, { Component } from "react";
import {
  View,
  StyleSheet
} from "react-native";
import Routing from "./src/routes/routing";

class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Routing />
      </View>
    );
  }
}
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});