import React, { Component } from "react";
import { Router, Stack, Scene, Drawer } from 'react-native-router-flux';
import { Root } from "native-base";

import Login from '../login';
import Splash from '../splash';
// // import Logout from '../logout';
 import Dashboard from '../pages/dashboard';
import TaskTabBar from "../pages/taskTabBar"
import InvestigationDetails from "../pages/InvestigationDetails"
import DeltaValue from "../pages/deltaValue"
import Report from "../pages/report"
import ReportPdfView from "../pages/reportPdfView"
import DownloadPdfReports from "../pages/downloadPdfReports"
import QuickApproval from "../pages/quickApproval"
class Routes extends Component {
    render() {
        return (
            <Router>
                <Stack key="root" hideNavBar={true}>
                    <Scene key="splash" component={Splash} title="Splash" initial={true} />
                    <Scene key="login" component={Login} title="Login" />
                     <Scene key="dashboard" component={Dashboard} title="Dashboard" hideNavBar/>
                      <Scene key="taskTabBar" component={TaskTabBar} title="Tasks" hideNavBar/>
                     <Scene key="investigationDetails" component={InvestigationDetails} title="InvestigationDetails" hideNavBar/>
                     <Scene key="deltaValue" component={DeltaValue} title="DeltaValue" hideNavBar/>
                      <Scene key="report" component={Report} title="Report" hideNavBar/>
                      <Scene key="reportPdfView" component={ReportPdfView} title="ReportPdfView" hideNavBar/>
                       <Scene key="downloadPdfReports" component={DownloadPdfReports} title="DownloadPdfReports" hideNavBar/>
                     <Scene key="quickApproval" component={QuickApproval} title="QuickApproval" hideNavBar/>  
                     {/* <Scene key="logout" component={Logout} title="Logout" />  */}
                </Stack>
            </Router>
        );
    }
}
export default () =>
    <Root>
        <Routes />
    </Root>