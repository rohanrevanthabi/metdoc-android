import React, { Component } from 'react';
import {  Alert, TextInput, View,Image, StyleSheet,TouchableOpacity,RefreshControl,BackHandler,Platform} from 'react-native';
import { Container, Icon,Header, Content, Button, Card,Right, CardItem, Body,Text,Toast } from 'native-base';
import TouchID from 'react-native-touch-id';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Actions } from "react-native-router-flux";
import DeviceInfo from 'react-native-device-info';
import * as url from "./url";
  class Login extends Component  {
  constructor(props) {
    super(props);
    
    this.state = {
      username: '',
      MobileNo: '',
      password: '',
      showPassword: true,
      fingerprintCancel:props.userCancel
    };
    this.backPressed = 0
  }
  componentDidMount = async () => {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.handleBackButton()
      return true;
    });
    // this.sessionExists();
  }
  componentWillUnmount() {
    this.backHandler.remove();
 }

 handleBackButton() {
  if (this.backPressed > 0) {
    setTimeout(() => BackHandler.exitApp(), 1000);
  } else {
    this.backPressed++;
    this.showToast("Press Again To Exit");
  } 
 }
 
  handleUsername = (text) => {
    this.setState({ username: text })
  }

  handleMobileNumber = (text) => {
    this.setState({ MobileNo: text })
  }

  handlePassword = (text) => {
    this.setState({ password: text })
  }
  
  loginSubmit = async (state) =>{
  //   if (state.username == '') {
  //     this.showToast("Please Enter Username");
  //  }
  //  else 
  if (state.MobileNo == '') {
    this.showToast("Please Enter MobileNo ");
 }
    else if (state.password == '') {
      this.showToast("Please Enter Password ");
   }
    else {
   // this.loginService(state);
    this.InstanceURL(state);
   }
  }
  //Service
  base_url = url.base_url;

  InstanceURL =async (state) => {
    var uniqueId = DeviceInfo.getUniqueId();
  fetch(this.base_url, {
      method: 'POST',
      headers: {
           Accept: 'application/json',
          'Content-Type': 'application/json',
           "ResWrapperReqYN":"Y"
      },
      body: JSON.stringify({
          UserName: state.username,
          Password: state.password,
          MobileNo:state.MobileNo,
          FCMInstanceId:"2345",
          DeviceId:uniqueId,
          AppVersionNo:"1.1"
      }),
  }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson,"URL");
          if (responseJson.status == "OK") {
              AsyncStorage.setItem('instanceURL', responseJson.response.Url);
              this.loginService(responseJson.response.Url,responseJson.response.LoginName,state);
          } else {
            this.showToast(responseJson.response);
          }
      })
      .catch((error) => {
          console.error(error);
      });
}
  loginService =async (url,username,state) => {
      var uniqueId = DeviceInfo.getUniqueId();
    fetch(url + "API/InvestigationValues/ValidateDoctor", {
        method: 'POST',
        headers: {
             Accept: 'application/json',
            'Content-Type': 'application/json',
             "ResWrapperReqYN":"Y"
        },
        body: JSON.stringify({
            UserName: username,
            Password: state.password,
            FCMInstanceId:"2345",
            DeviceId:uniqueId,
            AppVersionNo:"1.1"
        }),
    }).then((response) => response.json())
        .then((responseJson) => {
            if (responseJson.status == "OK") {
                AsyncStorage.setItem('SessionID', responseJson.response.FilePath);
                AsyncStorage.setItem('userName', responseJson.response.Name);
                AsyncStorage.setItem('LoginID', responseJson.response.LoginID.toString());
                AsyncStorage.setItem('orgId', responseJson.response.OrgID.toString());
                AsyncStorage.setItem('LocationId', responseJson.response.LocationID.toString());
                this.setNewUser(responseJson.response);
            } else {
              this.showToast(responseJson.response);
            }
        })
        .catch((error) => {
            console.error(error);
        });
  }

  setNewUser = async (user) => {
    var roleId = user.RoleID;
    var userId = user.UserID;
    if (user.LoginID) {
      AsyncStorage.setItem('roleId', roleId.toString());
      AsyncStorage.setItem('userId', userId.toString());
      if(this.state.fingerprintCancel=="User canceled authentication"){
        Actions.dashboard();
      }else{
        Actions.dashboard();
        const optionalConfigObject = {
          title: 'FingerPrint Required', // Android
          imageColor: '#038cfc', // Android
          imageErrorColor: '#e86868', // Android
          sensorDescription: 'Touch sensor', // Android
          sensorErrorDescription: 'Failed', // Android
          cancelText: 'Skip', // Android
          unifiedErrors: true, // use unified error messages (default false)
          fallbackLabel: '',
          passcodeFallback: false,// iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
        };
        if(Platform.OS === 'android'){
          TouchID.authenticate('' , optionalConfigObject)
          .then(success => {
            console.log(success,"--->success");
              this.showToast("FingerPrint Authenticated Successfully");
              AsyncStorage.setItem('FingerPrintSuccess',success.toString());
          })
          .catch(error => {
              console.log(error.message,"--->error finger");
              if(error.message=="User canceled authentication"){
                  AsyncStorage.setItem('FingerPrintSkiped',error.message);
                   this.showToast("User Skiped FingerPrint authentication");
                }
          });
        }else if(Platform.OS === 'ios'){
          TouchID.authenticate('If you press "Cancel" Button fingerprint will skiped in App' , optionalConfigObject)
          .then(success => {
            console.log(success,"--->success");
              this.showToast("FingerPrint Authenticated Successfully");
              AsyncStorage.setItem('FingerPrintSuccess',success.toString());
          })
          .catch(error => {
              console.log(error.message,"--->error finger");
              if(error.message=="User canceled authentication"){
                  AsyncStorage.setItem('FingerPrintSkiped',error.message);
                   this.showToast("User Skiped FingerPrint authentication");
                }
          });
        }
      }
    }
  }

  showToast = (message) => {
    Toast.show({
        text: message,
        buttonText: "Okay",
        duration: 3000
    })
  }

//Design
render() {
  return (
    <View style={styles.container}>
      <View style={{flex:1.5}}>
       <View style={{
         flex:0.9,
         backgroundColor:"#7387eb",
         borderBottomLeftRadius:50,
         borderBottomRightRadius:50
        }}>
       </View> 
      </View>
       <Card style={{flex:1,marginTop:-250,marginBottom:50, alignSelf:"center",width: 350,borderColor:"#eee"}}>
          <CardItem> 
            <Body>
              <Text style={styles.boldSigninText}>Sign In</Text>
              {/* <View style={{justifyContent: 'center'}}>
               <TextInput
                  ref={(input) => this.username = input}
                  onChangeText={this.handleUsername}
                  placeholder={'Username'}
                  placeholderTextColor="#a3a2a2" 
                  style={styles.input}
              />
              </View> */}
              <View style={{justifyContent: 'center'}}>
               <TextInput
                  ref={(input) => this.MobileNo = input}
                  onChangeText={this.handleMobileNumber}
                  placeholder={'MobileNumber'}
                  keyboardType='numeric'
                  placeholderTextColor="#a3a2a2" 
                  style={styles.input}
              />
              </View>
              <View style={{justifyContent: 'center',flexDirection: 'row',alignItems:"center"}}>
               <TextInput
                 ref={(input) => this.password = input}
                 onChangeText={this.handlePassword}
                 placeholder={'Password'}
                 placeholderTextColor="#a3a2a2" 
                 secureTextEntry={this.state.showPassword}
                 style={styles.input}
              />
               <Icon name={this.state.showPassword ? "ios-eye-off" : "ios-eye"} 
                     onPress={() => this.setState({ showPassword: !this.state.showPassword })}
               />
              </View>
              {/* <View style={styles.forgotTextCont}>
               <Text style={styles.forgot}>Forgot Password?</Text>
              </View> */}

                {Platform.OS === 'android'?
                <TouchableOpacity 
                  style={styles.loginBtn} 
                  onPress={() => this.loginSubmit(this.state)}
                  >
                <Text style={styles.loginBtnTxt}>
                  Sign In
                </Text>
                </TouchableOpacity>
               :Platform.OS === 'ios'?
               <TouchableOpacity 
                  style={styles.loginBtn} 
                  onPress={() => this.loginSubmit(this.state)}
                  >
                <Text style={styles.loginBtnTxt}>
                  Sign In
                </Text>
                </TouchableOpacity>:null
               }
            </Body>
         </CardItem>
       </Card>
    </View>
  );      
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  boldSigninText: {
    alignSelf:"center",
    fontSize: 25,
    fontWeight:"bold",
    color: 'black',
    padding:10
},
  input: {
    width: 290,
    height: 48,
    padding: 10,
    borderWidth: 1,
    borderBottomColor:'#eee',
    borderRightColor:'white',
    borderLeftColor:'white',
    borderTopColor:'white',
    textAlign:"left",
    marginBottom: 20,
    alignSelf:"center",
    color:"#000"

  },
  loginBtn:{
    backgroundColor:"#ed66aa",
    width: 290,
    borderRadius: 10,
    marginVertical: 10,
    paddingVertical: 13,
    alignSelf:"center"
  },
  loginBtnTxt:{
    fontSize: 16,
    fontWeight: '500',
    color: '#FFF',
    textAlign: 'center',
  },
  forgotTextCont: {
    alignSelf:"flex-end"
    
},
forgot: {
    fontSize: 16,
    color: '#000',
    padding: 5,
    fontWeight:"500",
},
click: {
    color: '#ec7063',
    fontSize: 16,
    padding: 5,
    fontWeight: '500'
}
});
export default Login;