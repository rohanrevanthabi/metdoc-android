import React, { Component } from 'react';
import { View,Text, StyleSheet ,SafeAreaView,FlatList,Image,BackHandler,TouchableOpacity,TextInput} from 'react-native';
import { Container, Header,Card, Content,CardItem, Left, Footer,Toast, FooterTab,Body, Right, Button, Icon, Title,Tab, Tabs,ScrollableTab } from 'native-base';
import { Actions } from "react-native-router-flux";
import * as url from "../url";
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import { SearchBar } from 'react-native-elements';
// import PDFReader from 'rn-pdf-reader-js'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
class Report extends Component {

constructor(props) {
    super(props);
    this.state = {
       getReports:"",
       searchBar:false,
       searchIcon:true,
       searchtext: '',
       newDate: new Date()
    };
    this.getReportsholder = [];
    }
    
 componentDidMount = async () => {
  this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
    Actions.pop();
    return true;
});
    this.GetInvestigationReports();
 
    }
 componentWillUnmount() {
    //  this.backHandler.remove();
    }
 showSearchIcon= () =>{
    this.setState({
    searchIcon: true,
    searchBar: false,
    });
 }    
 showSearchTextInput = () =>{
    this.setState({
    searchIcon: false,
    searchBar: true
    });
 }
 searchData(text) {
    const getReportData = this.getReportsholder.filter(item => {
    const pname = item.PatientName.toUpperCase();
    const vNumber = item.VisitNumber.toString()
    const textData = text.toUpperCase();
        return pname.indexOf(textData) > -1 || vNumber.indexOf(textData) > -1
   });   
    this.setState({
      getReports: getReportData,
      searchtext: text
    })
    this.GetInvestigationReports();
 }
 getPdfFile = async (item) => {
  const URL=await AsyncStorage.getItem('instanceURL');
  Actions.reportPdfView(
    {
      data: item,
      url:URL
    }
    );
}
 getDownloadPdfFile = async (item) => {
  const URL=await AsyncStorage.getItem('instanceURL');
  Actions.downloadPdfReports(
    {
      data: item,
      url:URL
    }
    );
}
//Service
 base_url = url.base_url;
 GetInvestigationReports = async () => {
  const URL=await AsyncStorage.getItem('instanceURL');
  const currentDate= moment(this.state.newDate).format('YYYYMMDD');
  const lastsevenDay=moment().subtract(7,"days").format('YYYYMMDD');
  const sessionId=await AsyncStorage.getItem('SessionID');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const roleId=await AsyncStorage.getItem('roleId');
      fetch(URL + "API/InvestigationValues/GetInvestigationReports?OrgID="+orgId+"&RoleID="+roleId+"&patientName=&fromDate="+lastsevenDay+"&toDate="+currentDate+"&StartRowIndex=1&PageSize=60", { 
      method: 'GET',
          headers: {
              'Content-Type': 'application/json',
              "ResWrapperReqYN":"Y",
              "LoginID":loginId,
              "SessionID":sessionId
          }
      }).then((response) => response.json())
          .then((responseJson) => {
             console.log(responseJson,"--->Res");
              if (responseJson.status =="OK") {
                this.setState({ getReports: responseJson.response.lstInvReports});
                this.getReportsholder = responseJson.response.lstInvReports;
              } else {
                 this.sessionExpired(responseJson);
              }
          })
          .catch((error) => {
              console.error(error);
          });
    }
    showToast = (message) => {
      Toast.show({
        text: message,
        buttonText: "Okay",
        duration: 3000
      })
    }
    sessionExpired = (responseJson) => {
      if (responseJson.status == "InternalServerError" ||
      responseJson.Message == "An error has occurred.") {
          AsyncStorage.clear();
          Actions.login();
          if (responseJson.status == "InternalServerError") {
            this.showToast(responseJson.status);
          }else if(responseJson.Message=="An error has occurred."){
            this.showToast("Session Expired");
          }
             
      }
  } 
//Design
  render() {
    return (
      <SafeAreaView  style={{flex:1}}>
      <Container>
         <Header style={{ backgroundColor:"#fff"}}>
        <Left style={{margin:10}}>
         <Title style={{color:"#000",fontWeight:"bold"}}>Reports</Title>
        </Left>
         <Right>
         {this.state.searchBar ==true ?
          <SearchBar
           placeholder="Search"
           placeholderTextColor={'#bdbdbd'}
           onChangeText={(text) => this.searchData(text)}
           value={this.state.searchtext}
           inputStyle={{backgroundColor: '#fff',color:"#000"}}
           inputContainerStyle={{backgroundColor:"#fff",borderColor:"red"}}
           containerStyle={{backgroundColor:'#fff',width:wp("80%"),borderTopColor:"#fff",borderBottomColor:"#ccc"}}
           clearIcon={{color:"#000"}}
           searchIcon={{color:"#000"}}
      />
            : null}
          <TouchableOpacity >
          {this.state.searchIcon ?
          <Icon onPress={this.showSearchTextInput} style={{ color:"#ccc"}} name='search' />:
          <Text onPress={this.showSearchIcon} style={{color:"#000",padding:15}}>Cancel</Text>}
          </TouchableOpacity >
         </Right>
        </Header>
        <Content>
        <View style={{ padding:15}}>
          {this.state.getReports.length == 0 ?
             <View style={styles.noDataFound}>
               <Text style={{ color: 'grey' }}>No Records Found</Text>
               </View> :
            <FlatList
             data={this.state.getReports}
             renderItem={({ item }) => ( 
               <TouchableOpacity onPress={this.getPdfFile.bind(this, item)}>
                <Card transparent style={styles.cardPatient} >
                <CardItem header style={styles.cardHeader}>
                <Left>
                    <Text style={{ fontWeight:"bold"}}>{item.VisitNumber}</Text>
                    </Left>
                  <Right>
                    <Text>{moment(item.VisitDate).format('DD MMM YYYY hh:mm A')}</Text>
                  </Right>
                </CardItem>
                  <CardItem  style={styles.cardBody}>
                    <Left>
                      <Image source={require("../image/person.png")}style={{height:50,width:50}}/>
                      <View>
                        <Text style={{margin:5, fontWeight:"bold"}}>{item.PatientName}</Text>
                        <Text style={{margin:5}}>{item.PatientAge}</Text>
                      </View>
                    </Left>
                  <Right>
                    <TouchableOpacity onPress={this.getDownloadPdfFile.bind(this, item)}>
                    <Image source={require("../image/download.png")}style={{height:40,width:40}}/>
                    </TouchableOpacity>
                 
                  </Right>
                </CardItem>
                <CardItem footer style={styles.cardFooter}>
                  <Text>{item.Location}</Text>
                </CardItem>
              </Card>
               </TouchableOpacity>   
            
               )}
               keyExtractor={(item, index) => index.toString()}
             />}
         </View>
        </Content>
        <Footer>
          <FooterTab style={{backgroundColor:"#f2f2f2"}}>
            <Button onPress={() => Actions.dashboard()} style={{flexDirection:"row"}}>
              <Icon style={{color:"#b3afaf"}} name="ios-home-outline" />
              <Text style={{color:"black"}}>Home</Text>
            </Button>
            <Button style={{flexDirection:"row"}}>
              <Icon style={{color:"#7387eb"}} name="ios-document" />    
              <Text style={{color:"#7387eb"}}>Report</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
      </SafeAreaView>
    );
  }
}
export default Report;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
  cardPatient:{ 
    alignSelf:"center",
    width: wp("95%"),
    backgroundColor:"#eee"
},
cardHeader:{
  backgroundColor: "#eee",
  borderBottomColor:"#fff",
  borderBottomWidth:3
},
cardBody:{
  backgroundColor: "#eee",
},
cardFooter:{
  backgroundColor: "#eee",
},
noDataFound: {
  alignItems: 'center',
  justifyContent: 'center',
}
});
