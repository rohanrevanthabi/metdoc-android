import React, { Component } from 'react';
import { View, Text, Image,ImageBackground,SafeAreaView ,Dimensions, StyleSheet,BackHandler,TextInput,ActivityIndicator,TouchableOpacity } from 'react-native';
import { Icon, Content, Container,Tabs,Tab,TabHeading,CardItem,Button, Body, Badge, Title, Toast,Accordion , Footer, FooterTab, Right, Left, Header,Subtitle,Thumbnail, Spinner } from 'native-base';
// import * as Font from 'expo-font';
// import { Ionicons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import * as LocalAuthentication from "expo-local-authentication";
import { Actions } from "react-native-router-flux";
import * as url from "../url";
import DropDownPicker from 'react-native-dropdown-picker';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { RefreshControl } from 'react-native';
import { Alert } from 'react-native';
class Dashboard extends Component {

   constructor(props) {
     super(props);
     this.state = {
       isLoading: true,
       searchtext: '',
       searchOpen:true,
       loading: true ,
       taskCount:"",
       summaryCount:"",
       category:"",
       patientDD:"vn",
       pendingPatient:[],
       delayPatient:[],
       criticalPatient:[],
       statPatient:[],
       coauthPatient:[],

     };
     this.backPressed = 0;
     this.noAvailable=""
     this.allPatientNames=[];
   }
 
   componentDidMount = async () => {
    // gradlew assembleRelease -x bundleReleaseJsAndAssets
        // const options  = {
        //   title: "Fingerprint Sensor",
        //   cancelButton: 'Cancel'
        // }
        // .isSensorAvailable()
        // .then(() => {
          // FingerprintScanner
          // // .isSensorAvailable()
          // .authenticate( options )
          // .then(() => {
          //   console.log('Authenticated successfully');
          // })
          // .catch((error) => {
          //    console.log(error.message,"--->ssss");
          //   if(error.message == "Authentication could not start because Fingerprint Scanner has no enrolled fingers."){
          //     this.showToast(error.message);
          //     Actions.login();
          //   }else if(error.message =="Authentication was canceled by the user - e.g. the user tapped Cancel in the dialog."){
          //     Actions.login();
          //   }
          //  });
          // })
          //Call the error method
        // .catch(error => onAuthenticationFailure(error))
     this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
       this.handleBackButton()
       return true;
     });
    // await Font.loadAsync({
    //   Roboto: require('native-base/Fonts/Roboto.ttf'),
    //   Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
    //   ...Ionicons.font,
    // });

    console.log("Dashboard page");
    this.setState({ loading: false });
    const name = await AsyncStorage.getItem('userName');
    this.setState({ username: name });
    console.log("user name set");
    this.getVistCount();

    
    this.getPendingPatientDetails();
    this.getDelayPatientDetails();
    this.getCriticalPatientDetails();
    this.getSTATPatientDetails();
    this.getCoAuthPatientDetails();
   }
 
   componentWillUnmount() {
   // FingerprintScanner.release();
      this.backHandler.remove();
   }
 
   handleBackButton() {
    if (this.backPressed > 0) {
      setTimeout(() => BackHandler.exitApp(), 1000);
    } else {
      this.backPressed++;
      this.showToast("Press Again To Exit");
    } 
   }

  searchData(text) {
    const textData = text.toUpperCase();
    this.setState({
      searchtext: text
      });
    }
    searchValue(data) {
     const textData = data.toUpperCase();
     console.log(this.allPatientNames,"--->PPP");
      if(this.allPatientNames.some(names =>
          (names.PatientName.toUpperCase() === textData ||names.VisitNumber.toString() === textData || names.MRN.toString() === textData ) && names.Category === "pending" )){
            Actions.taskTabBar
            (
              {
                search: data,
                searchpage:0,
                searchopen:true
              }
              )
          //  this.showToast("Patient Available");
         }else if(this.allPatientNames.some(names =>
          (names.PatientName.toUpperCase() === textData  ||names.VisitNumber.toString() === textData || names.MRN.toString() === textData) && names.Category === "delay" )){
            Actions.taskTabBar
            (
              {
                search: data,
                searchpage:1,
                searchopen:true
              }
              )
          //  this.showToast("Patient Available");
         }
         else if(this.allPatientNames.some(names =>
          (names.PatientName.toUpperCase() === textData  ||names.VisitNumber.toString() === textData || names.MRN.toString() === textData) && names.Category === "critical" )){
            Actions.taskTabBar
            (
              {
                search: data,
                searchpage:2,
                searchopen:true
              }
              )
          //  this.showToast("Patient Available");
         }
         else if(this.allPatientNames.some(names =>
          (names.PatientName.toUpperCase() === textData  ||names.VisitNumber.toString() === textData || names.MRN.toString() === textData) && names.Category === "stat" )){
            Actions.taskTabBar
            (
              {
                search: data,
                searchpage:3,
                searchopen:true
              }
              )
          //  this.showToast("Patient Available");
         }
         else if(this.allPatientNames.some(names =>
          (names.PatientName.toUpperCase() === textData  ||names.VisitNumber.toString() === textData || names.MRN.toString() === textData) && names.Category === "co-auth" )){
            Actions.taskTabBar
            (
              {
                search: data,
                searchpage:4,
                searchopen:true
              }
              )
          //  this.showToast("Patient Available");
         }
         else {
            this.showToast("Patient Not Available");
         }
     
    }
    taskBar(item) {
      Actions.taskTabBar({ data: item })
  }  
  onRefresh(){
    this.setState({
       isLoading:true,
       taskCount:"",
       summaryCount:"",
    });
    this.getVistCount();
  }
 //Service
 base_url = url.base_url;
 getVistCount = async () => {
  const sessionId=await AsyncStorage.getItem('SessionID');
  const URL=await AsyncStorage.getItem('instanceURL');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const roleId=await AsyncStorage.getItem('roleId');
  console.log("calling api");
      fetch(URL + "API/InvestigationValues/GetVisitCount?OrgID="+orgId+"&RoleID="+roleId+"&loginID="+loginId, { 
      method: 'GET',
          headers: {
              'Content-Type': 'application/json',
              "ResWrapperReqYN":"Y",
              "LoginID":loginId,
              "SessionID":sessionId
          }
      })
      .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson,"-->GetVisitCount");
              if (responseJson.status == "OK") {
                this.setState({ isLoading:false, taskCount: responseJson.response.TaskDetails });
                this.setState({ isLoading:false, summaryCount: responseJson.response.SummaryDetails });
              // } else if (responseJson.statusCode == 204) {
              //   this.setState({ isLoading:false, taskCount: 0 });
              //   this.setState({ isLoading:false, summaryCount: 0 });
              } else {
                 this.sessionExpired(responseJson);
              }
          })
          .catch((error) => {
              console.error(error);
          });
    }
    getPendingPatientDetails = async () => {
      const URL=await AsyncStorage.getItem('instanceURL');
      const sessionId=await AsyncStorage.getItem('SessionID');
      const loginId=await AsyncStorage.getItem('LoginID');
      const orgId=await AsyncStorage.getItem('orgId');
      const roleId=await AsyncStorage.getItem('roleId');
          fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=pending&Sort=&PatientName=", { 
          method: 'GET',
              headers: {
                  'Content-Type': 'application/json',
                  "ResWrapperReqYN":"Y",
                  "LoginID":loginId,
                  "SessionID":sessionId
              }
          }).then((response) => response.json())
              .then((responseJson) => {
                console.log(responseJson,"-->GetPendingpatientDetails");
                  if (responseJson.status =="OK") {
                    
                    this.setState({pendingPatient: responseJson.response});
                    this.state.pendingPatient.filter(item => {
                      this.allPatientNames.push(
                        {
                          PatientName:item.PatientName,
                          VisitNumber:item.VisitNumber,
                          MRN:item.PatientNumber,
                          Category:"pending" });
                    });
                    
                   
                  }
              })
              .catch((error) => {
                  console.error(error);
              });
        }
     getDelayPatientDetails = async () => {
       const sessionId=await AsyncStorage.getItem('SessionID');
       const URL=await AsyncStorage.getItem('instanceURL');
       const loginId=await AsyncStorage.getItem('LoginID');
       const orgId=await AsyncStorage.getItem('orgId');
       const roleId=await AsyncStorage.getItem('roleId');
        fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=tat&Sort=&PatientName=", { 
          method: 'GET',
            headers: {
             'Content-Type': 'application/json',
              "ResWrapperReqYN":"Y",
              "LoginID":loginId,
              "SessionID":sessionId
             }
             }).then((response) => response.json())
               .then((responseJson) => {
                console.log(responseJson,"-->getDelayPatientDetails");
                     
                if (responseJson.status =="OK") {
                
                 this.setState({delayPatient: responseJson.response });
                 this.state.delayPatient.filter(item => {
                  this.allPatientNames.push(
                    {
                    PatientName:item.PatientName,
                    VisitNumber:item.VisitNumber,
                    MRN:item.PatientNumber,
                    Category:"delay" }
                    );
                });
                } 
               })
                .catch((error) => {
                 console.error(error);
               });
            }   
     getCriticalPatientDetails = async () => {
        const sessionId=await AsyncStorage.getItem('SessionID');
        const URL=await AsyncStorage.getItem('instanceURL');
        const loginId=await AsyncStorage.getItem('LoginID');
        const orgId=await AsyncStorage.getItem('orgId');
        const roleId=await AsyncStorage.getItem('roleId');
        fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=critical&Sort=&PatientName=", {  
          method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              "ResWrapperReqYN":"Y",
              "LoginID":loginId,
              "SessionID":sessionId
              }
              }).then((response) => response.json())
                .then((responseJson) => {
                console.log(responseJson,"-->getCriticalPatientDetails");
                if (responseJson.status =="OK") {
                  this.setState({criticalPatient: responseJson.response });
                  this.state.criticalPatient.filter(item => {
                    this.allPatientNames.push(   
                      {
                        PatientName:item.PatientName,
                        VisitNumber:item.VisitNumber,
                        MRN:item.PatientNumber,
                        Category:"critical"
                       });
                  });
                } 
                })
                .catch((error) => {
                  console.error(error);
                });
          } 
     getSTATPatientDetails = async () => {
        const sessionId=await AsyncStorage.getItem('SessionID');
        const URL=await AsyncStorage.getItem('instanceURL');
        const loginId=await AsyncStorage.getItem('LoginID');
        const orgId=await AsyncStorage.getItem('orgId');
        const roleId=await AsyncStorage.getItem('roleId');
        fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=stat&Sort=&PatientName=", { 
          method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              "ResWrapperReqYN":"Y",
              "LoginID":loginId,
              "SessionID":sessionId
              }
              }).then((response) => response.json())
                .then((responseJson) => {
                  console.log(responseJson,"-->getSTATPatientDetails");
                      
                if (responseJson.status =="OK") {
                  this.setState({statPatient: responseJson.response });
                  this.state.statPatient.filter(item => {
                    this.allPatientNames.push(
                      {
                        PatientName:item.PatientName,
                        VisitNumber:item.VisitNumber,
                        MRN:item.PatientNumber,
                        Category:"stat" });
                  });
                } 
                })
                .catch((error) => {
                  console.error(error);
                });
        } 
     getCoAuthPatientDetails = async () => {
        const sessionId=await AsyncStorage.getItem('SessionID');
        const URL=await AsyncStorage.getItem('instanceURL');
        const loginId=await AsyncStorage.getItem('LoginID');
        const orgId=await AsyncStorage.getItem('orgId');
        const roleId=await AsyncStorage.getItem('roleId');
        fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=co-auth&Sort=&PatientName=", { 
          method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              "ResWrapperReqYN":"Y",
              "LoginID":loginId,
              "SessionID":sessionId
              }
              }).then((response) => response.json())
                .then((responseJson) => {
                  console.log(responseJson,"-->getCoAuthPatientDetails");
                if (responseJson.status =="OK") {
                  this.setState({coauthPatient: responseJson.response });
                  this.state.coauthPatient.filter(item => {
                    this.allPatientNames.push(
                      {
                        PatientName:item.PatientName,
                        VisitNumber:item.VisitNumber,
                        MRN:item.PatientNumber,
                        Category:"co-auth" });
                  });
                } 
                })
                .catch((error) => {
                  console.error(error);
                });
        }  
   showToast = (message) => {
      Toast.show({
        text: message,
        buttonText: "Okay",
        duration: 3000
      })
    }
    sessionExpired = (responseJson) => {
      if (responseJson.status == "InternalServerError" ||
      responseJson.Message == "An error has occurred.") {
          AsyncStorage.clear();
          Actions.login();
          if (responseJson.status == "InternalServerError") {
            this.showToast(responseJson.status);
          }else if(responseJson.Message=="An error has occurred."){
            this.showToast("Session Expired");
          }
             
      }
  } 
   //Design
   render() {
    if(this.state.isLoading){
       return(
         <View style={{flex:1,justifyContent:"center"}}>
           <ActivityIndicator size="large" color="#1E88E5"/>
         </View>
       );
    }
   return (
   <Container style={styles.container}>
      <Header style={{ backgroundColor:"#fff"}}>
         <View>
         <Left>
           <Title style={styles.welcomeText}>Welcome !</Title>
           <Subtitle style={styles.userName}>{this.state.username}</Subtitle>
         </Left>
         </View>
         <Right>
           <Button transparent>
             <Icon style={{ color:"#ccc"}}  name='ios-person' />
           </Button>
         </Right>
       </Header>
    
      <Content refreshControl={
        <RefreshControl
         refreshing={this.state.isLoading}
         onRefresh={this.onRefresh.bind(this)}
        />}style={{flex:1,padding:5,backgroundColor:"#fff"}}>
      <View style={{ flexDirection:"row",}}>
         <Left>
         {Platform.OS === 'android'?
           <DropDownPicker
           items={[
             { label: 'Visit Number', value: 'vn', selected: true },
             { label: 'Patient Name', value: 'pn' },
             { label: 'MRN', value: 'mrn' },
           ]}
           defaultValue={this.state.patientDD.value}
           style={{ backgroundColor: "#ccc" }}
           dropDownStyle={{ margin: 10 }}
           // placeholder="Select your country"
           containerStyle={{ width: wp('29%'), height: hp('9%'), padding: 10 }}
           onChangeItem={item => this.setState({
             patientDD: item.value
           })}
         />:Platform.OS === 'ios'?
            <DropDownPicker
           items={[
             {label: 'Visit Number', value: 'vn',selected: true},
             {label: 'Patient Name', value: 'pn'},
             {label: 'MRN', value: 'mrn'},
           ]}
           defaultValue={this.state.patientDD.value}
           style={{backgroundColor:"#ccc"}}
           dropDownStyle={{margin:10,height: 70}}
           // placeholder="Select your country"
            containerStyle={{width: (Dimensions.get('screen').width-260), height: (Dimensions.get('screen').width-300),padding:10}}
            onChangeItem={item => this.setState({
             patientDD: item.value
            })}
           /> 
            :null}
          </Left>
          {Platform.OS === 'android'?
            <Right style={{ marginRight: wp('45%'), flexDirection: "row", }}>
            <TextInput
              placeholder="Search Here"
              placeholderTextColor="#a3a2a2"
              onChangeText={(text) => this.searchData(text)}
              value={this.state.searchtext}
              style={styles.searchBar}>
            </TextInput>
            {this.state.searchtext != "" ?
              <TouchableOpacity onPress={this.searchValue.bind(this, this.state.searchtext)}>
                {/* {this.state.delayPatient.PatientName == this.state.searchtext ? */}
                <Text style={{ padding: 10, marginLeft: -5, fontWeight: "bold" }}>Go</Text>
                {/* :null} */}
              </TouchableOpacity>
              : null}
          </Right>:
            <Right style={{marginRight:(Dimensions.get('screen').width-220),flexDirection:"row",}}>    
            <TextInput
            placeholder="Search Here" 
            onChangeText={(text) => this.searchData(text)}
            value={this.state.searchtext}
            style= {styles.searchBarIos}>
            </TextInput>
            
             {this.state.searchtext!="" ?
             <TouchableOpacity onPress={this.searchValue.bind(this,this.state.searchtext)}>
               <Text  style={{padding:10,marginLeft:(Dimensions.get('screen').width-380),fontWeight:"bold"}}>Go</Text>
             </TouchableOpacity>
               :null}
           </Right>}
         
        </View>
          {/* vistCount */}
          {Platform.OS === 'android'?
          <ImageBackground source={require("../image/vistcountBG.png")}style={styles.vistCountBG}>
          <View style={{flexDirection:"row",justifyContent:"space-around"}}>
           <View style={{margin:15,alignItems:"center"}}>
            <Text style={styles.vistCount}> {this.state.summaryCount.Pending} </Text>
            <Text style={styles.vistCountName}> Pending </Text>
           </View>
            <Text style={{fontSize:50,color:"#c9c9c9"}}> | </Text>
           <View style={{margin:15,alignItems:"center"}}>
            <Text style={styles.vistCount}> {this.state.summaryCount.Approved} </Text>
            <Text style={styles.vistCountName}> Approval </Text>
           </View>
            <Text style={{fontSize:50,color:"#c9c9c9"}}> | </Text>
           <View style={{margin:15,alignItems:"center"}}>
            <Text style={styles.vistCount}> {this.state.summaryCount.Total} </Text>
            <Text style={styles.vistCountName}> Total </Text>
           </View>
          </View>
        </ImageBackground>:Platform.OS === 'ios'?
        <ImageBackground source={require("../image/vistcountBG.png")}style={styles.vistCountBGIos}>
          <View style={{flexDirection:"row",justifyContent:"space-around"}}>
           <View style={{margin:15,alignItems:"center"}}>
            <Text style={styles.vistCount}> {this.state.summaryCount.Pending} </Text>
            <Text style={styles.vistCountName}> Pending </Text>
           </View>
            <Text style={{fontSize:50,color:"#c9c9c9"}}> | </Text>
           <View style={{margin:15,alignItems:"center"}}>
            <Text style={styles.vistCount}> {this.state.summaryCount.Approved} </Text>
            <Text style={styles.vistCountName}> Approval </Text>
           </View>
            <Text style={{fontSize:50,color:"#c9c9c9"}}> | </Text>
           <View style={{margin:15,alignItems:"center"}}>
            <Text style={styles.vistCount}> {this.state.summaryCount.Total} </Text>
            <Text style={styles.vistCountName}> Total </Text>
           </View>
          </View>
        </ImageBackground>:null}
     {/* Task */}
     <View style={{padding:15}}>
       <Text style={styles.taskHeading}>
         Tasks
       </Text>
       <View style={{flexDirection:"row",justifyContent:"space-around",padding:15}}>
        <View>
        <TouchableOpacity
           onPress={this.taskBar.bind(this,0)}>
           <ImageBackground
             source={require("../image/pending.png")}
             style={styles.taskIcon}>
          {this.state.taskCount.Pending == 0 ?null:<Badge style={{alignSelf:"flex-end"}}>
           <Text style={{ color: '#FFF' }}>{this.state.taskCount.Pending}</Text>
          </Badge>}
         </ImageBackground>
        </TouchableOpacity>
          <Text style={styles.taskName}>Pending Approval</Text>
        </View>
         <View>
         <TouchableOpacity
           onPress={this.taskBar.bind(this,1)}>
         <ImageBackground source={require("../image/delayed.png")}style={styles.taskIcon}>
         {this.state.taskCount.DelayedTat == 0 ?null:<Badge style={{alignSelf:"flex-end"}}>
           <Text style={{ color: '#FFF' }}>{this.state.taskCount.DelayedTat}</Text>
           </Badge>}
         </ImageBackground>
         </TouchableOpacity>
         <Text style={styles.taskName}>Delayed Results</Text>
         </View>
         <View>
         <TouchableOpacity
           onPress={this.taskBar.bind(this,2)}>
         <ImageBackground source={require("../image/critical.png")}style={styles.taskIcon}>
         {this.state.taskCount.Critical == 0 ?null:<Badge style={{alignSelf:"flex-end"}}>
           <Text style={{ color: '#FFF' }}>{this.state.taskCount.Critical}</Text>
           </Badge>}
         </ImageBackground>
         </TouchableOpacity>
         <Text style={styles.taskName}>Critical Results</Text>
         </View>
       </View>
       <View style={{flexDirection:"row"}}>
       <View style={{ marginLeft: 38 }}>
       <TouchableOpacity
           onPress={this.taskBar.bind(this,3)}>
         <ImageBackground source={require("../image/STAT.png")}style={styles.taskIcon}>
         {this.state.taskCount.Stat == 0 ?null: <Badge style={{alignSelf:"flex-end"}}>
           <Text style={{ color: '#FFF' }}>{this.state.taskCount.Stat}</Text>
           </Badge>}
         </ImageBackground>
         </TouchableOpacity>
         <Text style={styles.taskName}>STAT Approval</Text>
         </View>
         <View style={{ marginLeft: 38 }}>
         <TouchableOpacity
           onPress={this.taskBar.bind(this,4)}>
         <ImageBackground source={require("../image/co-aut.png")}style={styles.taskIcon}>
         {this.state.taskCount.CoAuth == 0 ?null:<Badge style={{alignSelf:"flex-end"}}>
           <Text style={{ color: '#FFF' }}>{this.state.taskCount.CoAuth}</Text>
           </Badge>}
         </ImageBackground>
         </TouchableOpacity>
         <Text style={styles.taskName}>Co-Authorization</Text>
        </View>
       </View> 
     </View>
     <View style={{padding:15}}>
       <Text style={styles.Investigation}>
         Investigation
       </Text>
       <View style={{alignSelf:"flex-start",padding:15,marginLeft:25}}>
       <TouchableOpacity
          onPress={() => Actions.quickApproval()}>
         <ImageBackground source={require("../image/quickApproval.png")}style={styles.taskIcon}/>
         </TouchableOpacity>
         <Text style={styles.taskName}>Quick Approval</Text>
         </View>
       </View>
      </Content>
     <Footer>
         <FooterTab style={{backgroundColor:"#f2f2f2"}}>
           <Button style={{flexDirection:"row"}}>
             <Icon style={{color:"#7387eb"}} name="ios-home-outline" />
             <Text style={{color:"#7387eb"}}>Home</Text>
           </Button>
           <Button onPress={() => Actions.report()} style={{flexDirection:"row"}}>
             <Icon style={{color:"#b3afaf"}} name="ios-document" />    
             <Text style={{color:"black"}}>Report</Text>
           </Button>
         </FooterTab>
       </Footer>
   </Container>
  );
}


}
export default Dashboard;

const styles = StyleSheet.create ({
  container: {
     flex:1
  },
  welcomeText:{
     color: '#000',
     fontSize:17,
     fontWeight:"bold"
   },
   userName:{ 
     color: '#000',
     fontSize:15
   },
   searchBar:{
    width: wp("55%"),
    height: hp("6.5%"),
    borderWidth: 1,
    // paddingLeft: 20,
    // margin: 5,
    borderColor: '#eee',
    backgroundColor: '#eee',
    color: "#000"
},
   searchBarIos:{
     width: (Dimensions.get('screen').width-150),
     height: (Dimensions.get('screen').width-320),
     borderWidth: 1,
     // paddingLeft: 20,
     // margin: 5,
     borderColor: '#eee',
     backgroundColor: '#eee'
 },
  vistCountBG:{
   marginTop:20,
   width:wp("99%"),
   height:hp("10%"),
   alignSelf:"center",
  },
  vistCountBGIos:{
   marginTop:20,
   width:wp("99%"),
   height:(Dimensions.get('screen').width-300),
   alignSelf:"center",
   marginTop:60
  },
  vistCount:{
    color:"#fff",
    fontSize:27,
    fontWeight:"bold"
 },
  vistCountName:{
    color:"#fff",
    fontSize:12
},
  taskHeading:{
    fontWeight:"bold",
    fontSize:17
 },
   taskIcon:{
    width:wp("15%"),
    height:60,
    alignSelf:"center"
 },
  taskName:{
    textAlign:"center",
    fontSize:11
 },
 Investigation:{
   fontWeight:"bold",
   fontSize:17
},
  
})