import React, { Component } from 'react';
import { View,Text, StyleSheet ,SafeAreaView,FlatList,Image,TouchableOpacity,ActivityIndicator,RefreshControl, BackHandler,TextInput} from 'react-native';
import { Container, Header,Card, CardItem, Left, Body, Right, Button, Icon,Toast, Title,Tab, Tabs,ScrollableTab } from 'native-base';
import { Actions } from "react-native-router-flux";
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as url from "../url";
import { SearchBar } from 'react-native-elements';
import moment from 'moment';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
class TaskTabBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      category:props.data,
      pendingPatient:"",
      delayPatient:"",
      criticalPatient:"",
      statPatient:"",
      coauthPatient:"",
      
      searchBar:false,
      searchIcon:true,
      searchtext: props.search,
      searchpageView:props.searchpage,
      searchopen:props.searchopen
       
    };
    this.pendingholder = [];
    this.delayedholder=[];
    this.criticalholder=[];
    this.statholder=[];
    this.coauthholder=[];
    this.backPressed = 0
    this.initialPage=this.state.category
  }
  componentDidMount = async () => {
    
    if(this.state.searchopen){
      this.setState({
        searchIcon: false,
        searchBar: true
      });
      this.initialPage=this.state.searchpageView
    }
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      Actions.dashboard();
      return true;
  });
    this.getPendingPatientDetails();
    this.getDelayPatientDetails();
    this.getCriticalPatientDetails();
    this.getSTATPatientDetails();
    this.getCoAuthPatientDetails();
   }
   componentWillUnmount() {
    //  this.backHandler.remove();
   }
   showSearchIcon= () =>{
    this.setState({
      searchIcon: true,
      searchBar: false,
    });
   }
   showSearchTextInput = () =>{
    this.setState({
      searchIcon: false,
      searchBar: true
    });
  }
   searchData(text,page) {
    //  this.initialPage=page;
      const pendingData = this.pendingholder.filter(item => {
      const pname = item.PatientName.toUpperCase();
      const vNumber = item.VisitNumber.toString()
      const pId = item.PatientNumber.toString()
      const textData = text.toUpperCase();
      return pname.indexOf(textData) > -1 || vNumber.indexOf(textData) > -1 || pId.indexOf(textData) > -1
    });
    const delayData = this.delayedholder.filter(item => {
      const pname = item.PatientName.toUpperCase();
      const vNumber = item.VisitNumber.toString()
      const pId = item.PatientNumber.toString()
      const textData = text.toUpperCase();
      return pname.indexOf(textData) > -1 || vNumber.indexOf(textData) > -1 || pId.indexOf(textData) > -1
    });
    const criticalData = this.criticalholder.filter(item => {
      const pname = item.PatientName.toUpperCase();
      const vNumber = item.VisitNumber.toString()
      const pId = item.PatientNumber.toString()
      const textData = text.toUpperCase();
      return pname.indexOf(textData) > -1 || vNumber.indexOf(textData) > -1 || pId.indexOf(textData) > -1
    });
    const statData = this.statholder.filter(item => {
      const pname = item.PatientName.toUpperCase();
      const vNumber = item.VisitNumber.toString()
      const pId = item.PatientNumber.toString()
      const textData = text.toUpperCase();
      return pname.indexOf(textData) > -1 || vNumber.indexOf(textData) > -1 || pId.indexOf(textData) > -1
    });
    const coauthData = this.coauthholder.filter(item => {
      const pname = item.PatientName.toUpperCase();
      const vNumber = item.VisitNumber.toString()
      const pId = item.PatientNumber.toString()
      const textData = text.toUpperCase();
      return pname.indexOf(textData) > -1 || vNumber.indexOf(textData) > -1 || pId.indexOf(textData) > -1
    });
 
   this.setState({
      pendingPatient: pendingData,
      delayPatient: delayData,
      criticalPatient: criticalData,
      statPatient: statData,
      coauthPatient: coauthData,
      searchtext: text
      })
    }
    getVisitId(item) {
      Actions.investigationDetails({data: item});
  }
  onRefreshPending(){
    this.setState({
       isLoading:true,
       pendingPatient:"",
    });
    this.getPendingPatientDetails();
    this.initialPage=0;
  }
  onRefreshDelay(){
    this.setState({
       isLoading:true,
       delayPatient:"",
      
    });
    this.getDelayPatientDetails();
    this.initialPage=1;
  }
  onRefreshCritical(){
    this.setState({
       isLoading:true,
       criticalPatient:"",
      
    });
    this.getCriticalPatientDetails();
    this.initialPage=2;
  }
  onRefreshSTAT(){
    this.setState({
       isLoading:true,
       statPatient:"",
      
    });
    this.getSTATPatientDetails();
    this.initialPage=3;
  }
  onRefreshCoAuth(){
    this.setState({
       isLoading:true,
       tcoauthPatient:"",
    });
    this.getCoAuthPatientDetails();
    this.initialPage=4;
  }
//Service
   base_url = url.base_url;
   getPendingPatientDetails = async () => {
    const URL=await AsyncStorage.getItem('instanceURL');
    const sessionId=await AsyncStorage.getItem('SessionID');
    const loginId=await AsyncStorage.getItem('LoginID');
    const orgId=await AsyncStorage.getItem('orgId');
    const roleId=await AsyncStorage.getItem('roleId');
        fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=pending&Sort=&PatientName=", { 
        method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                "ResWrapperReqYN":"Y",
                "LoginID":loginId,
                "SessionID":sessionId
            }
        }).then((response) => response.json())
            .then((responseJson) => {
               
                if (responseJson.status =="OK") {
                  this.setState({ isLoading:false, pendingPatient: responseJson.response });
                      this.pendingholder = responseJson.response;
                      if(this.props.search!="" && this.props.search!= undefined ){
                        this.searchData(this.state.searchtext);
                      }
                      //console.log(this.state.pendingPatient,"--->Pp");
                  
                } else {
                   this.sessionExpired(responseJson);
                }
            })
            .catch((error) => {
                console.error(error);
            });
      }
   getDelayPatientDetails = async () => {
    const URL=await AsyncStorage.getItem('instanceURL');
     const sessionId=await AsyncStorage.getItem('SessionID');
     const loginId=await AsyncStorage.getItem('LoginID');
     const orgId=await AsyncStorage.getItem('orgId');
     const roleId=await AsyncStorage.getItem('roleId');
      fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=tat&Sort=&PatientName=", { 
        method: 'GET',
          headers: {
           'Content-Type': 'application/json',
            "ResWrapperReqYN":"Y",
            "LoginID":loginId,
            "SessionID":sessionId
           }
           }).then((response) => response.json())
             .then((responseJson) => {
                   
              if (responseJson.status =="OK") {
               this.setState({ isLoading:false,delayPatient: responseJson.response });
               this.delayedholder = responseJson.response;
               if(this.props.search!="" && this.props.search!= undefined ){
                this.searchData(this.state.searchtext);
              }
               console.log(this.state.delayPatient,"--->Dp");
              } 
             })
              .catch((error) => {
               console.error(error);
             });
          }   
   getCriticalPatientDetails = async () => {
      const URL=await AsyncStorage.getItem('instanceURL');
      const sessionId=await AsyncStorage.getItem('SessionID');
      const loginId=await AsyncStorage.getItem('LoginID');
      const orgId=await AsyncStorage.getItem('orgId');
      const roleId=await AsyncStorage.getItem('roleId');
      fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=critical&Sort=&PatientName=", { 
        method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            "ResWrapperReqYN":"Y",
            "LoginID":loginId,
            "SessionID":sessionId
            }
            }).then((response) => response.json())
              .then((responseJson) => {
                    
              if (responseJson.status =="OK") {
                this.setState({ isLoading:false,criticalPatient: responseJson.response });
                this.criticalholder = responseJson.response;
                if(this.props.search!="" && this.props.search!= undefined ){
                  this.searchData(this.state.searchtext);
                }
              } else {
                this.sessionExpired(responseJson);
              }
              })
              .catch((error) => {
                console.error(error);
              });
        } 
   getSTATPatientDetails = async () => {
      const URL=await AsyncStorage.getItem('instanceURL');
      const sessionId=await AsyncStorage.getItem('SessionID');
      const loginId=await AsyncStorage.getItem('LoginID');
      const orgId=await AsyncStorage.getItem('orgId');
      const roleId=await AsyncStorage.getItem('roleId');
      fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=stat&Sort=&PatientName=", { 
        method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            "ResWrapperReqYN":"Y",
            "LoginID":loginId,
            "SessionID":sessionId
            }
            }).then((response) => response.json())
              .then((responseJson) => {
                    
              if (responseJson.status =="OK") {
                this.setState({ isLoading:false,statPatient: responseJson.response });
                this.statholder = responseJson.response;
                if(this.props.search!="" && this.props.search!= undefined ){
                  this.searchData(this.state.searchtext);
                }
              } 
              })
              .catch((error) => {
                console.error(error);
              });
      } 
   getCoAuthPatientDetails = async () => {
      const URL=await AsyncStorage.getItem('instanceURL');
      const sessionId=await AsyncStorage.getItem('SessionID');
      const loginId=await AsyncStorage.getItem('LoginID');
      const orgId=await AsyncStorage.getItem('orgId');
      const roleId=await AsyncStorage.getItem('roleId');
      fetch(URL + "API/InvestigationValues/GetPendingpatientDetails?OrgID="+orgId+"&loginID="+loginId+"&RoleID="+roleId+"&VisitID=0&category=co-auth&Sort=&PatientName=", { 
        method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            "ResWrapperReqYN":"Y",
            "LoginID":loginId,
            "SessionID":sessionId
            }
            }).then((response) => response.json())
              .then((responseJson) => {
                    
              if (responseJson.status =="OK") {
                this.setState({ isLoading:false,coauthPatient: responseJson.response });
                this.coauthPatient = responseJson.response;
                if(this.props.search!="" && this.props.search!= undefined ){
                  this.searchData(this.state.searchtext);
                }
              } 
              })
              .catch((error) => {
                console.error(error);
              });
      }     
      showToast = (message) => {
        Toast.show({
          text: message,
          buttonText: "Okay",
          duration: 3000
        })
      }
      sessionExpired = (responseJson) => {
        if (responseJson.status == "InternalServerError" ||
        responseJson.Message == "An error has occurred.") {
            AsyncStorage.clear();
            Actions.login();
            if (responseJson.status == "InternalServerError") {
              this.showToast("Session Expired");
            }else if(responseJson.Message=="An error has occurred."){
              this.showToast("Session Expired");
            }
               
        }
    }      
//Design
   render() {
    if(this.state.isLoading){
      return(
        <View style={{flex:1,justifyContent:"center"}}>
          <ActivityIndicator size="large" color="#1E88E5"/>
        </View>
      );
   }
    return (
      <SafeAreaView  style={{flex:1}}>
      <Container>
      <Header style={{ backgroundColor:"#7387eb"}}>
        <Left>
          <Button transparent>
          {Platform.OS === 'android'?
            <Icon name='arrow-back' 
              onPress={() => Actions.dashboard()}
            />:Platform.OS === 'ios'?
            <Icon name='arrow-back' style={{ color:"#FFF"}}
            onPress={() => Actions.dashboard()}
          />:null}
          </Button>
        </Left>
        <Body>
        {Platform.OS === 'android'?
          <Title>Tasks</Title>:
          Platform.OS === 'ios'?
          <Title style={{ color:"#FFF"}}>Tasks</Title>:null}
        </Body>
         <Right>
          {this.state.searchBar ==true ?
          <SearchBar
           placeholder="Search"
           placeholderTextColor={'#eee'}
           onChangeText={(text) => this.searchData(text)}
           value={this.state.searchtext}
           inputStyle={{backgroundColor: '#7387eb',color:"#fff"}}
           inputContainerStyle={{backgroundColor:"#7387eb"}}
           containerStyle={{backgroundColor: '#7387eb',width:wp("80%"),borderTopColor:"#7387eb"}}
           clearIcon={{color:"#fff"}}
           searchIcon={{color:"#fff"}}
      />
            : null}
          <TouchableOpacity >
          {this.state.searchIcon ?
          <Icon onPress={this.showSearchTextInput} style={{ color:"#fff"}} name='search' />:
          <Text onPress={this.showSearchIcon} style={{color:"#fff",padding:15}}>Cancel</Text>}
          </TouchableOpacity >
        </Right>
      </Header>
      <Tabs initialPage={this.initialPage} tabBarUnderlineStyle={{borderBottomWidth:4,borderBottomColor:"#ed66aa"}}>
        <Tab heading="PENDING" 
            tabStyle={{backgroundColor: '#fff'}}
            textStyle={{color: '#aaadaa',fontSize:13}}
            activeTabStyle={{backgroundColor: '#fff'}}
            activeTextStyle={{color: '#000',fontSize:13, fontWeight: 'normal'}}>
            <View style={{ padding:15}}>
            {this.state.pendingPatient.length == 0 ?
                <View style={styles.noDataFound}>
                    <Text style={{ color: 'grey' }}>No Pending Available </Text>
                </View> :
              <FlatList
                data={this.state.pendingPatient}
                renderItem={({ item }) => (
              <Card transparent style={styles.cardPatient}>
                <CardItem header style={styles.cardHeader}>
                  <Left>
                    <Text style={{ fontWeight:"bold"}}>{item.VisitNumber}</Text>
                    </Left>
                  <Right>
                    <Text>{moment(item.VisitDate).format('DD MMM YYYY hh:mm A')}</Text>
                  </Right>
                </CardItem>
                  <CardItem style={styles.cardBody}>
                    <Left>
                      <Image source={require("../image/person.png")}style={{height:50,width:50}}/>
                      <View>
                        <Text style={{margin:5, fontWeight:"bold"}}>{item.PatientName}</Text>
                        <Text style={{margin:5}}>{item.SEX}/{item.AGE}</Text>
                      </View>
                    </Left>
                    
                  <Right>
                    <Icon onPress={this.getVisitId.bind(this, item)} style={{ color:"#6496e8",fontSize:40}} name='arrow-forward'/>
                  </Right>
                </CardItem>
                <CardItem footer style={styles.cardFooter}>
                  <Text>{item.Location}</Text>
                </CardItem>
              </Card>
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                  <RefreshControl
                   refreshing={this.state.isLoading}
                   onRefresh={this.onRefreshPending.bind(this)}
                  />}
              />}
            </View>
        </Tab>
          <Tab heading="DELAY" 
                tabStyle={{backgroundColor: '#fff'}}
                textStyle={{color: '#aaadaa',fontSize:13}}
                activeTabStyle={{backgroundColor: '#fff'}}
                activeTextStyle={{color: '#000',fontSize:13, fontWeight: 'normal'}}>
                <View style={{ padding:15}}>
                {this.state.delayPatient.length == 0 ?
                <View style={styles.noDataFound}>
                    <Text style={{ color: 'grey' }}>No Delayed Available </Text>
                </View> :
              <FlatList
                data={this.state.delayPatient}
                renderItem={({ item }) => (
              <Card transparent style={styles.cardPatient}>
                <CardItem header style={styles.cardHeader}>
                  <Left>
                    <Text style={{ fontWeight:"bold"}}>{item.VisitNumber}</Text>
                    </Left>
                  <Right>
                    <Text>{moment(item.VisitDate).format('DD MMM YYYY hh:mm A')}</Text>
                  </Right>
                </CardItem>
                  <CardItem style={styles.cardBody}>
                    <Left>
                      <Image source={require("../image/person.png")}style={{height:50,width:50}}/>
                      <View>
                        <Text style={{margin:5, fontWeight:"bold"}}>{item.PatientName}</Text>
                        <Text style={{margin:5}}>{item.SEX}/{item.AGE}</Text>
                      </View>
                    </Left>
                    
                  <Right>
                    <Icon onPress={this.getVisitId.bind(this, item)} style={{ color:"#6496e8",fontSize:40}} name='arrow-forward'/>
                  </Right>
                </CardItem>
                <CardItem footer style={styles.cardFooter}>
                  <Text>{item.Location}</Text>
                </CardItem>
              </Card>
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                  <RefreshControl
                   refreshing={this.state.isLoading}
                   onRefresh={this.onRefreshDelay.bind(this)}
                  />}
              />}
            </View>
          </Tab>
          <Tab heading="CRITICAL" 
                tabStyle={{backgroundColor: '#fff'}}
                textStyle={{color: '#aaadaa',fontSize:13}}
                activeTabStyle={{backgroundColor: '#fff'}}
                activeTextStyle={{color: '#000',fontSize:13, fontWeight: 'normal'}}>
                <View style={{ padding:15}}>
                {this.state.criticalPatient.length == 0 ?
                <View style={styles.noDataFound}>
                    <Text style={{ color: 'grey' }}>No Critical Available </Text>
                </View> :
              <FlatList
                data={this.state.criticalPatient}
                renderItem={({ item }) => (
              <Card transparent style={styles.cardPatient}>
                <CardItem header style={styles.cardHeader}>
                  <Left>
                    <Text style={{ fontWeight:"bold"}}>{item.VisitNumber}</Text>
                    </Left>
                  <Right>
                    <Text>{moment(item.VisitDate).format('DD MMM YYYY hh:mm A')}</Text>
                  </Right>
                </CardItem>
                  <CardItem style={styles.cardBody}>
                    <Left>
                      <Image source={require("../image/person.png")}style={{height:50,width:50}}/>
                      <View>
                        <Text style={{margin:5, fontWeight:"bold"}}>{item.PatientName}</Text>
                        <Text style={{margin:5}}>{item.SEX}/{item.AGE}</Text>
                      </View>
                    </Left>
                    
                  <Right>
                    <Icon onPress={this.getVisitId.bind(this, item)} style={{ color:"#6496e8",fontSize:40}} name='arrow-forward'/>
                  </Right>
                </CardItem>
                <CardItem footer style={styles.cardFooter}>
                  <Text>{item.Location}</Text>
                </CardItem>
              </Card>
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                  <RefreshControl
                   refreshing={this.state.isLoading}
                   onRefresh={this.onRefreshCritical.bind(this)}
                  />}
              />}
            </View>
          </Tab>
        
          <Tab heading="STAT" 
                tabStyle={{backgroundColor: '#fff'}}
                textStyle={{color: '#aaadaa',fontSize:13}}
                activeTabStyle={{backgroundColor: '#fff'}}
                activeTextStyle={{color: '#000',fontSize:13, fontWeight: 'normal'}}>
              <View style={{ padding:15}}>
                {this.state.statPatient.length == 0 ?
                <View style={styles.noDataFound}>
                    <Text style={{ color: 'grey' }}>No STAT Available </Text>
                </View> :
              <FlatList
                data={this.state.statPatient}
                renderItem={({ item }) => (
              <Card transparent style={styles.cardPatient}>
                <CardItem header style={styles.cardHeader}>
                  <Left>
                    <Text style={{ fontWeight:"bold"}}>{item.VisitNumber}</Text>
                    </Left>
                  <Right>
                    <Text>{moment(item.VisitDate).format('DD MMM YYYY hh:mm A')}</Text>
                  </Right>
                </CardItem>
                  <CardItem style={styles.cardBody}>
                    <Left>
                      <Image source={require("../image/person.png")}style={{height:50,width:50}}/>
                      <View>
                        <Text style={{margin:5, fontWeight:"bold"}}>{item.PatientName}</Text>
                        <Text style={{margin:5}}>{item.SEX}/{item.AGE}</Text>
                      </View>
                    </Left>
                    
                  <Right>
                    <Icon onPress={this.getVisitId.bind(this, item)} style={{ color:"#6496e8",fontSize:40}} name='arrow-forward'/>
                  </Right>
                </CardItem>
                <CardItem footer style={styles.cardFooter}>
                  <Text>{item.Location}</Text>
                </CardItem>
              </Card>
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                  <RefreshControl
                   refreshing={this.state.isLoading}
                   onRefresh={this.onRefreshSTAT.bind(this)}
                  />}
              />}
            </View>
          </Tab>
          <Tab heading="CO-AUTH" 
                tabStyle={{backgroundColor: '#fff'}}
                textStyle={{color: '#aaadaa',fontSize:13}}
                activeTabStyle={{backgroundColor: '#fff'}}
                activeTextStyle={{color: '#000',fontSize:13, fontWeight: 'normal'}}>
              <View style={{ padding:15}}>
                {this.state.coauthPatient.length == 0 ?
                <View style={styles.noDataFound}>
                    <Text style={{ color: 'grey' }}>No Co-Auth Available </Text>
                </View> :
              <FlatList
                data={this.state.coauthPatient}
                renderItem={({ item }) => (
              <Card transparent style={styles.cardPatient}>
                <CardItem header style={styles.cardHeader}>
                  <Left>
                    <Text style={{ fontWeight:"bold"}}>{item.VisitNumber}</Text>
                    </Left>
                  <Right>
                    <Text>{moment(item.VisitDate).format('DD MMM YYYY hh:mm A')}</Text>
                  </Right>
                </CardItem>
                  <CardItem style={styles.cardBody}>
                    <Left>
                      <Image source={require("../image/person.png")}style={{height:50,width:50}}/>
                      <View>
                        <Text style={{margin:5, fontWeight:"bold"}}>{item.PatientName}</Text>
                        <Text style={{margin:5}}>{item.SEX}/{item.AGE}</Text>
                      </View>
                    </Left>
                    
                  <Right>
                    <Icon onPress={this.getVisitId.bind(this, item)} style={{ color:"#6496e8",fontSize:40}} name='arrow-forward'/>
                  </Right>
                </CardItem>
                <CardItem footer style={styles.cardFooter}>
                  <Text>{item.Location}</Text>
                </CardItem>
              </Card>
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                  <RefreshControl
                   refreshing={this.state.isLoading}
                   onRefresh={this.onRefreshCoAuth.bind(this)}
                  />}
              />}
            </View>
          </Tab>
          </Tabs>
    </Container>
    </SafeAreaView>
    );
}
}
export default TaskTabBar;
const styles = StyleSheet.create({
  cardPatient:{ 
    alignSelf:"center",
    width: wp("95%"),
    backgroundColor:"#eee"
},
cardHeader:{
  backgroundColor: "#eee",
  borderBottomColor:"#fff",
  borderBottomWidth:3
},
cardBody:{
  backgroundColor: "#eee",
},
cardFooter:{
  backgroundColor: "#eee",
},
noDataFound: {
  alignItems: 'center',
  justifyContent: 'center',
}
});