import React, { Component } from 'react';
import { View,Text, StyleSheet ,FlatList,Image,BackHandler,Alert,TouchableOpacity,ActivityIndicator,TextInput} from 'react-native';
import { Container, Header,Card, Content,CardItem, Left, Footer,Toast, FooterTab,Body, Right, Button, Icon, Title,Tab, Tabs,ScrollableTab } from 'native-base';
import { Actions } from "react-native-router-flux";
import * as url from "../url";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DropDownPicker from 'react-native-dropdown-picker';
import moment from 'moment';
import { SearchBar } from 'react-native-elements';
// import PDFReader from 'rn-pdf-reader-js'
import PDFView from 'react-native-view-pdf';
// import base64 from 'react-native-base64'
class Report extends Component {

constructor(props) {
    super(props);
    this.state = {
        isLoading: true,
        getPdf:[],
        getPdfTask:[],
        totalRows:"",
        quantity:1,
        //inputCount:"",
        goCount:"",
        inputValue:"",
        investigationName:[],
        departmentList:[],
        selectedDepartment:{}
    };
    this.pdf = null;
    }
    
  componentDidMount = async () => {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      Actions.pop();
      return true;
    }); 
    // this.getQuickApprovalPDF();
    this.getDepartmentList();
  }

 componentWillUnmount() {
    //  this.backHandler.remove();
    }
    
  incrementCount = async () => {
    this.setState(prevState => {
      // return {
      //   quantity: prevState.quantity + 1
      // }
      if(prevState.quantity < this.state.totalRows) {
        return {
          quantity: Number(prevState.quantity) + Number(1)
        }
      } else {
        return null;
      }
    });
    this.getQuickApprovalPDF();
}

  decrementCount = async () => {
    this.setState(prevState => {
      if(prevState.quantity > 1) {
        return {
          quantity: Number(prevState.quantity) - Number(1)
        }
      } else {
        return null;
      }
    });
    this.getQuickApprovalPDF();
  }
  
  inputPdfIndex = async (text) => {
    this.setState({ goCount: text });
    // this.goButton(this.state.quantity);
   }
   goButton=async (quantity) => {
     this.setState({ quantity: quantity });
    //  this.getinputCountlPDF(this.state.quantity);
     this.getQuickApprovalPDF();
     this.setState({ goCount: "" });
      // console.log(this.)
   }
   getVisitId(item) {
    Actions.investigationDetails({data: item});
}
hideSpinner() {
  this.setState({ isLoading: false });

}
displayError() {
  Alert.alert(
    "No Internet",
    "Require Internet connection",
    [
      { text: 'OK', onPress: () => this.getQuickApprovalPDF()},
    ],
    { cancelable: false });
}
displayPDFError() {
  Alert.alert(
    "PDF Error",
    "PDF is Invalid",
    [
      { text: 'OK', onPress: () => console.log("okay")},
    ],
    { cancelable: false });
}
//Service
base_url = url.base_url;

getQuickApprovalPDF = async (resetIndexQuantity = false) => {
  const URL=await AsyncStorage.getItem('instanceURL');
  const sessionId=await AsyncStorage.getItem('SessionID');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const roleId=await AsyncStorage.getItem('roleId');
  this.setState({ isLoading: true });
  if(resetIndexQuantity) {
    this.setState({ quantity: 1});
  }
  console.log(URL + "API/InvestigationValues/GetQuickApproval?OrgID="+orgId+"&RoleID="+roleId+"&LoginID="+loginId+"&StartRowIndex="+this.state.quantity+"&PageSize=1"+"&DeptId="+this.state.selectedDepartment.value);

      fetch(URL + "API/InvestigationValues/GetQuickApproval?OrgID="+orgId+"&RoleID="+roleId+"&LoginID="+loginId+"&StartRowIndex="+this.state.quantity+"&PageSize=1"+"&DeptId="+this.state.selectedDepartment.value, { 
      method: 'GET',
          headers: {
              'Content-Type': 'application/json',
              "ResWrapperReqYN":"Y",
              "LoginID":loginId,
              "SessionID":sessionId
          }
      }).then((response) => response.json())
          .then((responseJson) => {
             console.log(responseJson,"-->Resp getQuickApprovalPDF: " + this.state.selectedDepartment.label);
              if (responseJson.status =="OK") {
                this.setState({ totalRows: responseJson.response.TotalRows});
                this.setState({ isLoading:false, getPdf: responseJson.response.ReportDetails});
                this.setState({ getPdfTask: responseJson.response.TaskDetails});
                //console.log(this.state.getPdfTask,"--->dsdsdd")
                this.getInvesetigationValues(this.state.getPdfTask);
              } else {
                this.sessionExpired(responseJson);
                this.hideSpinner();
              }
          })
          .catch((error) => {
              this.hideSpinner();
              console.error(error);
          });
    }

    getDepartmentList = async () => {
    const URL=await AsyncStorage.getItem('instanceURL');
    const sessionId=await AsyncStorage.getItem('SessionID');
    const loginId=await AsyncStorage.getItem('LoginID');
    const orgId=await AsyncStorage.getItem('orgId');
    const roleId=await AsyncStorage.getItem('roleId');
    this.setState({ isLoading: true });
    console.log(URL + "API/InvestigationValues/GetDepartmentByID?OrgID="+orgId+"&RoleID="+roleId+"&LoginID="+loginId);

      fetch(URL + "API/InvestigationValues/GetDepartmentByID?OrgID="+orgId+"&RoleID="+roleId+"&LoginID="+loginId, { 
      method: 'GET',
          headers: {
              'Content-Type': 'application/json',
              "ResWrapperReqYN":"Y",
              "LoginID":loginId,
              "SessionID":sessionId
          }
      }).then((response) => response.json())
          .then((responseJson) => {
             console.log(responseJson,"-->Resp");
              if (responseJson.status =="OK") {
                if(responseJson.response && responseJson.response.length > 0) {
                  var list = responseJson.response;
                  for(let i=0; i<list.length; i++) {
                    list[i].value = list[i].DeptID;
                    list[i].label = list[i].DeptName;
                  }
                  this.setState({ departmentList: list});
                  this.setState({ selectedDepartment: list[0]});
                }
                this.getQuickApprovalPDF();
              } else {
                 this.sessionExpired(responseJson);
              }
          })
          .catch((error) => {
              this.hideSpinner();
              console.error(error);
          });
    }

getinputCountlPDF = async (quantity) => {                     //not using
  const URL=await AsyncStorage.getItem('instanceURL');
  const sessionId=await AsyncStorage.getItem('SessionID');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const roleId=await AsyncStorage.getItem('roleId');
  this.setState({ isLoading: true });
  console.log(URL + "API/InvestigationValues/GetQuickApproval?OrgID="+orgId+"&RoleID="+roleId+"&LoginID="+loginId+"&StartRowIndex="+quantity+"&PageSize=1");
      fetch(URL + "API/InvestigationValues/GetQuickApproval?OrgID="+orgId+"&RoleID="+roleId+"&LoginID="+loginId+"&StartRowIndex="+quantity+"&PageSize=1", { 
      method: 'GET',
          headers: {
              'Content-Type': 'application/json',
              "ResWrapperReqYN":"Y",
              "LoginID":loginId,
              "SessionID":sessionId
          }
      }).then((response) => response.json())
          .then((responseJson) => {
              if (responseJson.status =="OK") {
                this.setState({ totalRows: responseJson.response.TotalRows});
                this.setState({ isLoading:false, getPdf: responseJson.response.ReportDetails})
                this.setState({ getPdfTask: responseJson.response.TaskDetails});
                this.getInvesetigationValues(this.state.getPdfTask);
               // console.log(this.state.getPdf,"--->dsdsdd")
              } else {
                  this.sessionExpired(responseJson);
              }
          })
          .catch((error) => {
              this.hideSpinner();
              console.error(error);
          });
    }

getInvesetigationValues= async (patientVisitID) =>{
  const URL=await AsyncStorage.getItem('instanceURL');
  const sessionId=await AsyncStorage.getItem('SessionID');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const roleId=await AsyncStorage.getItem('roleId');
 for (let i = 0; i < patientVisitID.length; i++) {  
    fetch(
      URL + "API/InvestigationValues/GetPatientInvValues?LoginID="+loginId+"&OrgID="+orgId+"&VisitID="+patientVisitID[i].PatientVisitID+"&RoleID="+roleId+"",
      { 
      method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          "ResWrapperReqYN":"Y",
          "LoginID":loginId,
          "SessionID":sessionId
          }
          }).then((response) => response.json())
            .then((responseJson) => { 
              
            // console.log("getInvesetigationValues", responseJson);
            if (responseJson.status =="OK") {
              this.setState({ investigationName: responseJson.response.TestDetailsList});
              this.hideSpinner();
              // console.log(this.state.investigationName,"-->investigationName");
      
            } else {
              this.sessionExpired(responseJson);
            }
            })
            .catch((error) => {
              console.error(error);
              this.hideSpinner();
            });
          }
}
submitApproveAll =async () => {  
  const currentDate= moment(this.state.newDate).format('YYYYMMDD hh:mm:ss');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const locationId=await AsyncStorage.getItem('LocationId');
  const roleId=await AsyncStorage.getItem('roleId');
   var obj ;
   var  lstInv_RefPatientValues=[];
   for (let i = 0; i < this.state.investigationName.length; i++) { 
     if(this.state.investigationName[i].TestType=="INV"){
      for (let p = 0; p < this.state.getPdfTask.length; p++) { 
      lstInv_RefPatientValues.push(
        {
          "InvestigationName":this.state.investigationName[i].TestName,
          "InvestigationID":this.state.investigationName[i].TestID,
          "GroupID":this.state.investigationName[i].GroupID,
          "GroupName":"",
          "GroupComment":"",
          "PatientVisitID":this.state.investigationName[i].PatientVisitID,
          "CreatedBy":loginId,
          "CollectedDateTime":currentDate,
          "Status":"Approve",
          "ComplaintID":this.state.investigationName[i].ComplaintId,
          "Type":this.state.investigationName[i].Type,
          "OrgID":orgId,
          "InvestigationMethodID":this.state.investigationName[i].InvestigationMethodID,
          "MethodName":"",
          "KitID":this.state.investigationName[i].KitID,
          "KitName":"",
          "InstrumentID":this.state.investigationName[i].InstrumentID,
          "InstrumentName":"",
          "Interpretation":"",
          "PrincipleID":this.state.investigationName[i].PrincipleID,
          "PrincipleName":"",
          "QCData":"",
          "InvestigationSampleContainerID":this.state.investigationName[i].InvestigationSampleContainerID,
          "PackageID":this.state.investigationName[i].PackageID,
          "PackageName":"",
          "Reason":this.state.investigationName[i].Reason,
          "ReportStatus":"",
          "ReferenceRange":this.state.investigationName[i].ReferenceRange,
          "PerformingPhysicainName":"",
          "ApprovedBy":loginId,
          "GUID":this.state.getPdfTask[p].GUID,
          "IsAbnormal":this.state.investigationName[i].IsAbnormal,
          "AccessionNumber":this.state.investigationName[i].AccessionNumber,
          "AutoApproveLoginID":this.state.investigationName[i].AutoApproveLoginID,
          "ValidatedBy":this.state.investigationName[i].ValidatedBy,
          "RemarksID":this.state.investigationName[i].RemarksID,
          "MedicalRemarks":this.state.investigationName[i].MedicalRemarks,
          "GroupMedicalRemarks":"",
          "InvSampleStatusID":this.state.investigationName[i].InvSampleStatusID,
          "AuthorizedBy":this.state.investigationName[i].AuthorizedBy,
          "ConvReferenceRange":this.state.investigationName[i].ConversionRange,
          "ManualAbnormal":this.state.investigationName[i].ManualAbnormal,
          "IsAutoAuthorize":this.state.investigationName[i].IsAutoAuthorize,
          "PrintableRange":"",
          "IsAutoValidate":"",
          "InvStatusReasonID":this.state.investigationName[i].InvStatusReasonID,
          "IsSensitive":this.state.investigationName[i].IsSensitive,
          "IsReportable":this.state.investigationName[i].IsReportable,
          "Value":this.state.investigationName[i].TestValue,
          "Dilution":"",
          "DeviceID":"",
          "DeviceValue":"",
          "DeviceActualValue":"" ,
          "ConvValue":this.state.investigationName[i].ConversionValue,
          "UOMCode":this.state.investigationName[i].UOMCode,
          "CONV_UOMCode":this.state.investigationName[i].ConversionUnit
        });
      }

     }else if(this.state.investigationName[i].TestType=="GRP"){
      for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
        if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo==null){
        for (let p = 0; p < this.state.getPdfTask.length; p++) { 
        lstInv_RefPatientValues.push(
          {
            "InvestigationName":this.state.investigationName[i].OrderContentListInfo[j].TestName,
            "InvestigationID":this.state.investigationName[i].OrderContentListInfo[j].TestID,
            "GroupID":this.state.investigationName[i].OrderContentListInfo[j].OrgGroupID,
            "GroupName":this.state.investigationName[i].OrderContentListInfo[j].GroupName,
            "GroupComment":"",
            "PatientVisitID":this.state.investigationName[i].PatientVisitID,
            "CreatedBy":loginId,
            "CollectedDateTime":currentDate,
            "Status":"Approve",
            "ComplaintID":this.state.investigationName[i].OrderContentListInfo[j].ComplaintId,
            "Type":this.state.investigationName[i].OrderContentListInfo[j].Type,
            "OrgID":orgId,
            "InvestigationMethodID":this.state.investigationName[i].OrderContentListInfo[j].InvestigationMethodID,
            "MethodName":"",
            "KitID":this.state.investigationName[i].OrderContentListInfo[j].KitID,
            "KitName":"",
            "InstrumentID":this.state.investigationName[i].OrderContentListInfo[j].InstrumentID,
            "InstrumentName":"",
            "Interpretation":"",
            "PrincipleID":this.state.investigationName[i].OrderContentListInfo[j].PrincipleID,
            "PrincipleName":"",
            "QCData":"",
            "InvestigationSampleContainerID":this.state.investigationName[i].OrderContentListInfo[j].InvestigationSampleContainerID,
            "PackageID":this.state.investigationName[i].OrderContentListInfo[j].PackageID,
            "PackageName":"",
            "Reason":this.state.investigationName[i].OrderContentListInfo[j].Reason,
            "ReportStatus":"",
            "ReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ReferenceRange,
            "PerformingPhysicainName":"",
            "ApprovedBy":loginId,
            "GUID":this.state.getPdfTask[p].GUID,
            "IsAbnormal":this.state.investigationName[i].OrderContentListInfo[j].IsAbnormal,
            "AccessionNumber":this.state.investigationName[i].OrderContentListInfo[j].AccessionNumber,
            "AutoApproveLoginID":this.state.investigationName[i].OrderContentListInfo[j].AutoApproveLoginID,
            "ValidatedBy":this.state.investigationName[i].OrderContentListInfo[j].ValidatedBy,
            "RemarksID":this.state.investigationName[i].OrderContentListInfo[j].RemarksID,
            "MedicalRemarks":this.state.investigationName[i].OrderContentListInfo[j].MedicalRemarks,
            "GroupMedicalRemarks":"",
            "InvSampleStatusID":this.state.investigationName[i].OrderContentListInfo[j].InvSampleStatusID,
            "AuthorizedBy":this.state.investigationName[i].OrderContentListInfo[j].AuthorizedBy,
            "ConvReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ConversionRange,
            "ManualAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ManualAbnormal,
            "IsAutoAuthorize":this.state.investigationName[i].OrderContentListInfo[j].IsAutoAuthorize,
            "PrintableRange":"",
            "IsAutoValidate":"",
            "InvStatusReasonID":this.state.investigationName[i].OrderContentListInfo[j].InvStatusReasonID,
            "IsSensitive":this.state.investigationName[i].OrderContentListInfo[j].IsSensitive,
            "IsReportable":this.state.investigationName[i].OrderContentListInfo[j].IsReportable,
            "Value":this.state.investigationName[i].OrderContentListInfo[j].TestValue,
            "Dilution":"",
            "DeviceID":"",
            "DeviceValue":"",
            "DeviceActualValue":"" ,
            "ConvValue":this.state.investigationName[i].OrderContentListInfo[j].ConversionValue,
            "UOMCode":this.state.investigationName[i].OrderContentListInfo[j].UOMCode,
            "CONV_UOMCode":this.state.investigationName[i].OrderContentListInfo[j].ConversionUnit
          });
        }
      }else if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo!=null){
        for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
          for (let p = 0; p < this.state.getPdfTask.length; p++) { 
            lstInv_RefPatientValues.push(
              {
                "InvestigationName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestName,
                "InvestigationID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID,
                "GroupID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].OrgGroupID,
                "GroupName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].GroupName,
                "GroupComment":"",
                "PatientVisitID":this.state.investigationName[i].PatientVisitID,
                "CreatedBy":loginId,
                "CollectedDateTime":currentDate,
                "Status":"Approve",
                "ComplaintID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ComplaintId,
                "Type":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Type,
                "OrgID":orgId,
                "InvestigationMethodID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvestigationMethodID,
                "MethodName":"",
                "KitID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].KitID,
                "KitName":"",
                "InstrumentID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InstrumentID,
                "InstrumentName":"",
                "Interpretation":"",
                "PrincipleID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PrincipleID,
                "PrincipleName":"",
                "QCData":"",
                "InvestigationSampleContainerID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvestigationSampleContainerID,
                "PackageID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PackageID,
                "PackageName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PackageName,
                "Reason":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason,
                "ReportStatus":"",
                "ReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ReferenceRange,
                "PerformingPhysicainName":"",
                "ApprovedBy":loginId,
                "GUID":this.state.getPdfTask[p].GUID,
                "IsAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsAbnormal,
                "AccessionNumber":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AccessionNumber,
                "AutoApproveLoginID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AutoApproveLoginID,
                "ValidatedBy":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ValidatedBy,
                "RemarksID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].RemarksID,
                "MedicalRemarks":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].MedicalRemarks,
                "GroupMedicalRemarks":"",
                "InvSampleStatusID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvSampleStatusID,
                "AuthorizedBy":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AuthorizedBy,
                "ConvReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ConversionRange,
                "ManualAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ManualAbnormal,
                "IsAutoAuthorize":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsAutoAuthorize,
                "PrintableRange":"",
                "IsAutoValidate":"",
                "InvStatusReasonID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvStatusReasonID,
                "IsSensitive":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsSensitive,
                "IsReportable":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsReportable,
                "Value":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestValue,
                "Dilution":"",
                "DeviceID":"",
                "DeviceValue":"",
                "DeviceActualValue":"" ,
                "ConvValue":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ConversionValue,
                "CONV_UOMCode":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ConversionUnit,
                "UOMCode":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].UOMCode
              });
          }
        }
      }
      }
     }else if(this.state.investigationName[i].TestType=="PKG"){
      for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
        if(this.state.investigationName[i].OrderContentListInfo[j].TestType=="INV"){
          for (let p = 0; p < this.state.getPdfTask.length; p++) { 
          lstInv_RefPatientValues.push(
            {
              "InvestigationName":this.state.investigationName[i].OrderContentListInfo[j].TestName,
              "InvestigationID":this.state.investigationName[i].OrderContentListInfo[j].TestID,
              "GroupID":this.state.investigationName[i].OrderContentListInfo[j].GroupID,
              "GroupName":this.state.investigationName[i].OrderContentListInfo[j].GroupName,
              "GroupComment":"",
              "PatientVisitID":this.state.investigationName[i].PatientVisitID,
              "CreatedBy":loginId,
              "CollectedDateTime":currentDate,
              "Status":"Approve",
              "ComplaintID":this.state.investigationName[i].OrderContentListInfo[j].ComplaintId,
              "Type":this.state.investigationName[i].OrderContentListInfo[j].Type,
              "OrgID":orgId,
              "InvestigationMethodID":this.state.investigationName[i].OrderContentListInfo[j].InvestigationMethodID,
              "MethodName":"",
              "KitID":this.state.investigationName[i].OrderContentListInfo[j].KitID,
              "KitName":"",
              "InstrumentID":this.state.investigationName[i].OrderContentListInfo[j].InstrumentID,
              "InstrumentName":"",
              "Interpretation":"",
              "PrincipleID":this.state.investigationName[i].OrderContentListInfo[j].PrincipleID,
              "PrincipleName":"",
              "QCData":"",
              "InvestigationSampleContainerID":this.state.investigationName[i].OrderContentListInfo[j].InvestigationSampleContainerID,
              "PackageID":this.state.investigationName[i].OrderContentListInfo[j].PackageID,
              "PackageName":this.state.investigationName[i].OrderContentListInfo[j].PackageName,
              "Reason":this.state.investigationName[i].OrderContentListInfo[j].Reason,
              "ReportStatus":"",
              "ReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ReferenceRange,
              "PerformingPhysicainName":"",
              "ApprovedBy":loginId,
              "GUID":this.state.getPdfTask[p].GUID,
              "IsAbnormal":this.state.investigationName[i].OrderContentListInfo[j].IsAbnormal,
              "AccessionNumber":this.state.investigationName[i].OrderContentListInfo[j].AccessionNumber,
              "AutoApproveLoginID":this.state.investigationName[i].OrderContentListInfo[j].AutoApproveLoginID,
              "ValidatedBy":this.state.investigationName[i].OrderContentListInfo[j].ValidatedBy,
              "RemarksID":this.state.investigationName[i].OrderContentListInfo[j].RemarksID,
              "MedicalRemarks":this.state.investigationName[i].OrderContentListInfo[j].MedicalRemarks,
              "GroupMedicalRemarks":"",
              "InvSampleStatusID":this.state.investigationName[i].OrderContentListInfo[j].InvSampleStatusID,
              "AuthorizedBy":this.state.investigationName[i].OrderContentListInfo[j].AuthorizedBy,
              "ConvReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ConversionRange,
              "ManualAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ManualAbnormal,
              "IsAutoAuthorize":this.state.investigationName[i].OrderContentListInfo[j].IsAutoAuthorize,
              "PrintableRange":"",
              "IsAutoValidate":"",
              "InvStatusReasonID":this.state.investigationName[i].OrderContentListInfo[j].InvStatusReasonID,
              "IsSensitive":this.state.investigationName[i].OrderContentListInfo[j].IsSensitive,
              "IsReportable":this.state.investigationName[i].OrderContentListInfo[j].IsReportable,
              "Value":this.state.investigationName[i].OrderContentListInfo[j].TestValue,
              "Dilution":"",
              "DeviceID":"",
              "DeviceValue":"",
              "DeviceActualValue":"" ,
              "ConvValue":this.state.investigationName[i].OrderContentListInfo[j].ConversionValue,
              "CONV_UOMCode":this.state.investigationName[i].OrderContentListInfo[j].ConversionUnit,
              "UOMCode":this.state.investigationName[i].OrderContentListInfo[j].UOMCode
              
            });
          }
        }else if(this.state.investigationName[i].OrderContentListInfo[j].TestType=="GRP" && this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo!=null){
          for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) {
            for (let p = 0; p < this.state.getPdfTask.length; p++) {
            lstInv_RefPatientValues.push(
              {
                "InvestigationName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestName,
                "InvestigationID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID,
                // "GroupID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].GroupID,
                "GroupID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].OrgGroupID,
                "GroupName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].GroupName,
                "GroupComment":"",
                "PatientVisitID":this.state.investigationName[i].PatientVisitID,
                "CreatedBy":loginId,
                "CollectedDateTime":currentDate,
                "Status":"Approve",
                "ComplaintID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ComplaintId,
                "Type":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Type,
                "OrgID":orgId,
                "InvestigationMethodID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvestigationMethodID,
                "MethodName":"",
                "KitID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].KitID,
                "KitName":"",
                "InstrumentID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InstrumentID,
                "InstrumentName":"",
                "Interpretation":"",
                "PrincipleID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PrincipleID,
                "PrincipleName":"",
                "QCData":"",
                "InvestigationSampleContainerID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvestigationSampleContainerID,
                "PackageID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PackageID,
                "PackageName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PackageName,
                "Reason":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason,
                "ReportStatus":"",
                "ReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ReferenceRange,
                "PerformingPhysicainName":"",
                "ApprovedBy":loginId,
                "GUID":this.state.getPdfTask[p].GUID,
                "IsAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsAbnormal,
                "AccessionNumber":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AccessionNumber,
                "AutoApproveLoginID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AutoApproveLoginID,
                "ValidatedBy":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ValidatedBy,
                "RemarksID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].RemarksID,
                "MedicalRemarks":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].MedicalRemarks,
                "GroupMedicalRemarks":"",
                "InvSampleStatusID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvSampleStatusID,
                "AuthorizedBy":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AuthorizedBy,
                "ConvReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ConversionRange,
                "ManualAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ManualAbnormal,
                "IsAutoAuthorize":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsAutoAuthorize,
                "PrintableRange":"",
                "IsAutoValidate":"",
                "InvStatusReasonID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvStatusReasonID,
                "IsSensitive":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsSensitive,
                "IsReportable":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsReportable,
                "Value":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestValue,
                "Dilution":"",
                "DeviceID":"",
                "DeviceValue":"",
                "DeviceActualValue":"" ,
                "ConvValue":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ConversionValue,
                "CONV_UOMCode":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ConversionUnit,
                "UOMCode":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].UOMCode
              });
            }
          }
        }
      }
    }
   }
    for (let p = 0; p < this.state.getPdfTask.length; p++) { 
      obj=({
        "ApprovadBy":loginId,
        "LocationID":locationId,
        "OrgID":orgId,
        "PatientID":this.state.getPdfTask[p].PatientID,
        "RoleID":roleId,
        "TaskID":this.state.getPdfTask[p].TaskID,
        "VisitID":this.state.getPdfTask[p].PatientVisitID,
        "deptID":0,
        "gUID":this.state.getPdfTask[p].GUID,
    
        "lstInvestigationValues":lstInv_RefPatientValues,
        "lstReflexPatientinvestigation":lstInv_RefPatientValues
     })
  }

 this.Investigation(obj);
}

Investigation =async (obj) => {
  const URL=await AsyncStorage.getItem('instanceURL');
  const sessionId=await AsyncStorage.getItem('SessionID');
  const loginId=await AsyncStorage.getItem('LoginID');
  // const orgId=await AsyncStorage.getItem('orgId');
  // const locationId=await AsyncStorage.getItem('LocationId');
  // const roleId=await AsyncStorage.getItem('roleId');
   console.log(obj,"--->GRP");
  fetch(URL + "API/InvestigationValues/SaveInvestigationResults", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "ResWrapperReqYN":"Y",
        "LoginID":loginId,
        "SessionID":sessionId
        },
        body: JSON.stringify(obj)
  }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson,"--->xxcvs");
        if (responseJson.status =="OK" && responseJson.response =="Success") {
          if(this.state.getPdf.length!=0){
            this.showToast("Approved Successfully");
            this.getQuickApprovalPDF()
          }else{
            this.showToast("Approved Successfully");
            Actions.dashboard();
          }
          
          
          }else {
            this.sessionExpired(responseJson);
          }
      })
      .catch((error) => {
          console.error(error);
      });
}
    showToast = (message) => {
      Toast.show({
        text: message,
        buttonText: "Okay",
        duration: 3000
      })
    }
    sessionExpired = (responseJson) => {
      if (responseJson.status == "InternalServerError" ||
      responseJson.Message == "An error has occurred.") {
          if (responseJson.status == "InternalServerError") {
            this.showToast(responseJson.status);
          }else if(responseJson.Message=="An error has occurred."){
            AsyncStorage.clear();
            Actions.login();
            this.showToast("Session Expired");
          }
             
      }
  } 
//Design
  render() {
        if(this.state.isLoading){
          return(
            <View style={{flex:1,justifyContent:"center"}}>
              <ActivityIndicator size="large" color="#1E88E5"/>
            </View>
          );
      }
     ;

     var departmentList = this.state.departmentList;
        var pdfListItems =  this.state.getPdf.map((item,index) => {
          
          const resources = {
            file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
            // url: source,
            base64: item.Content,
          };
          const resourceType = 'base64'
          return (
             <View style={styles.container} key={index}>  
             {/* <PDFReader
                ref={(pdf)=>{this.pdf = pdf;}}
                key={index}
                source={{ base64: "data:application/pdf;base64,"+item.Content+""}}
                withScroll={true}
                withPinchZoom={true}
                style={{ flex: 1 }}
                onLoad={() => this.hideSpinner()}
                onError={() => this.displayError()}
           /> */}
           <PDFView
              fadeInDuration={250.0}
              style={{ flex: 1 }}
              key={index}
              resource={resources[resourceType]}
              resourceType={resourceType}
              onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
              // onError={(error) => console.log('Cannot render PDF', error)}
              onError={(error) =>  {
                console.log('Cannot render PDF', error);
                this.displayPDFError();
              }}
            />
            <Footer>
             <FooterTab style={{backgroundColor:"#ffff",}}>
             <FlatList
              data={this.state.getPdfTask}
              renderItem={({ item, index}) => (
               <Button onPress={this.getVisitId.bind(this, item)} key={index} style={{alignSelf:"flex-start",backgroundColor:"#ccc",width:140}}>
                 <Text key={index} style={{color:"#ed66aa",fontWeight:"bold"}}>Edit</Text>
               </Button>
               )}
               keyExtractor={(item, index) => index.toString()}
             />
               <Button onPress={this.submitApproveAll} key={index} style={{alignSelf:"flex-start",backgroundColor:"#ed66aa",width:150}}>
                 <Text key={index} style={{color:"#fff",fontWeight:"bold"}}>Approve</Text>
               </Button>
             </FooterTab>
           </Footer>
            </View>
          )
        });
       
    return (
      <Container>
          <Header style={{ backgroundColor:"#7387eb"}}>
         <Left>
        <Icon onPress={() => Actions.pop()} style={{ color:"#fff"}} name="arrow-back"/>
        </Left>
        <Body>
        {Platform.OS === 'android'?
        <Title style={{ fontSize:15}}>Quick Approval</Title>:
        Platform.OS === 'ios'?
        <Title style={{ fontSize:15,color:"#FFF"}}>Quick Approval</Title>:null}
        </Body>
        <Right >
         <Icon onPress={() => Actions.dashboard()} 
               style={{ color:"#fff"}} 
               name="home-outline"
         />
        </Right>
        </Header>  

        <View style={{ flexDirection:"row",}}>
          <Left>
          {Platform.OS === 'android'?
            <DropDownPicker
            items={departmentList}
            defaultValue={this.state.selectedDepartment.value}
            style={{ backgroundColor: "#ccc" }}
            dropDownStyle={{ margin: 10 }}
            labelStyle={{ flex : 1 }}
            //  placeholder="Select Depertment"
            containerStyle={{ width: wp('49%'), height: hp('9%'), padding: 10 }}
            onChangeItem={item => {
              this.setState({
                selectedDepartment: item
              });
              this.getQuickApprovalPDF(true);
            }}
          />:Platform.OS === 'ios'?
              <DropDownPicker
            items={departmentList}
            defaultValue={this.state.selectedDepartment.value}
            style={{backgroundColor:"#ccc"}}
            dropDownStyle={{margin:10,height: 70}}
            labelStyle={{ flex : 1 }}
            // placeholder="Select your country"
              containerStyle={{width: (Dimensions.get('screen').width-260), height: (Dimensions.get('screen').width-300),padding:10}}
              onChangeItem={item => {
                this.setState({
                  selectedDepartment: item
                });
                this.getQuickApprovalPDF(true);
              }}
            /> 
              :null}
            </Left>
          </View>

        {this.state.getPdf.length!=0?  <View  style={styles.nextOperation}>
           <View>
              <Text> Patient {this.state.quantity} of {this.state.totalRows} </Text>
           </View>
          <View style={{flexDirection:"row"}}>
          <TextInput
                placeholder={"00"}
                onChangeText={this.inputPdfIndex}
                ref={(input) => this.goCount = input}
                value={this.state.goCount}
                style={styles.pdfSearch}
              />
             {this.state.goCount!=""?
             <Button primary style={{margin:5,width:40,height:30}} onPress={this.goButton.bind(this,this.state.goCount)}>
             <Text style={{color:"#fff",textAlign:"center"}} > GO </Text>
             </Button>:
             <Button disabled light style={{margin:5,width:40,height:30}}>
             <Text style={{color:"#fff",textAlign:"center"}} > GO </Text>
             </Button>
              }
          </View>
          <View style={{flexDirection:"row"}}>
            <TouchableOpacity onPress={this.decrementCount.bind(this)}>
            <Image source={require("../image/back.png")} style={{width:40,height:40}}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.incrementCount.bind(this)}>
            <Image source={require("../image/forward.png")}style={{width:40,height:40}}/>
            </TouchableOpacity>
          </View>
        </View>:
            <Text style={{textAlign:"center",justifyContent:"center"}}>No data Found</Text>}
              {pdfListItems}   
      </Container>
    
    );

  }
}
export default Report;
const styles = StyleSheet.create({
  container: {
        flex: 1,
        backgroundColor: '#ecf0f1',  
  },
  cardPatient:{ 
    alignSelf:"center",
    width: 390,
    backgroundColor:"#eee"
  },
  cardHeader:{
    backgroundColor: "#eee",
    borderBottomColor:"#fff",
    borderBottomWidth:3
  },
  cardBody:{
    backgroundColor: "#eee",
  },
  cardFooter:{
    backgroundColor: "#eee",
  },
  noDataFound: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  pdf: {
      flex:1,
  },
  nextOperation:{
    padding:5,
    backgroundColor:"#fff",
    flexDirection:"row",
    justifyContent:"space-evenly",
  },
  pdfSearch:{
    width: 60,
    height: 40,
    padding: 10,
    borderWidth: 2,
    borderRadius:5,
    borderBottomColor:'#ccc',
    borderRightColor:'#ccc',
    borderLeftColor:'#ccc',
    borderTopColor:'#ccc',
    textAlign:"center",
    marginBottom: 20,
    alignSelf:"center",
    backgroundColor:"#fff"
  },
});
