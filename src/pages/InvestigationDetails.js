import React, { Component } from 'react';
import { View,Text, StyleSheet ,SafeAreaView,FlatList,Image,TouchableOpacity,BackHandler, Modal,TextInput} from 'react-native';
import { Container, Header,Card, Content,CardItem,checked, Left,Toast, Footer,ActionSheet, FooterTab,Body, Right, Button, Icon,Subtitle, Title,Tab, Tabs,ScrollableTab } from 'native-base';
import { Actions } from "react-native-router-flux";
import AsyncStorage from '@react-native-async-storage/async-storage';
// import { List } from 'react-native-paper';
// import Accordian from '../pages/accordion'
import * as url from "../url";
import { CheckBox } from 'react-native-elements'
import Dialog from "react-native-dialog";
import DropDownPicker from 'react-native-dropdown-picker';
import moment from 'moment';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
// import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import Menu, {
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
class InvestigationDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
        patientInfo:props.data,
        uID:"",
        patientName:"",
        visitNumber:"",
        checkbox: false,
        button:[],
        packageButton:[],
        groupName:[],
        investigationName:[],
        longPress:[],
        checkclick:[],
        editValue:"",
        editValueId:"",
        editReferenceRange:"",
        medicalComment:"",
        technicalComment:"",
        commentValueId:"",
        recollect_rerun:"",
        editPopupvisible:false,
        commentPopupVisable:false,
        infoPopupVisable:false,
        infoPopupValue:"",
        medicalExpand:false,
        techincalExpand:false,
        actionVisible:false,
        recollectVisable:false,
        rerunVisable:false,
        actionValue:"",
        recollectReson:"",
        recollectId:"",
        rerunReson:"",
        rerunId:"",
        viewEye:[],
        packageViewEye:[],
        checkclickItem:[],
        reasonName:[],
        actionPopUp:[],
    };
    this.expandedBlock=[];
    this.packageExpandedBlock=[];
    this.checked=[];
    this.packageChecked=[];
    this.pressCard=[];
    this.packagePressCard=[];
    this.viewEnable=[];
    this.packageViewEnable=[];
    this.invactioMENU=[];
    this.grpactioMENU=[];
    this.pakgrpactioMENU=[];
    this.allCheckClick=false;
    this.subviewEnable=false;
    this.reasonStatus=[];
    //  this.allApproveInvestigation();
    }
    componentDidMount() {
      this.setState({patientInfo : this.state.patientInfo});  
      console.log(this.state.patientInfo,"--->patientInfo");
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
          Actions.pop();
          return true;
      });
      this.getInvesetigationValues();
      this.getReason();
  }
  componentWillUnmount() {
    //  this.backHandler.remove();
   }
expanded= (id) =>{
     this.expandedBlock[id] = !this.expandedBlock[id];
     this.viewEnable[id] =true;
    this.setState({
      button : this.expandedBlock[id],
      viewEye:this.viewEnable[id]
    });
   }

packageExpanded= (id) =>{
  this.packageExpandedBlock[id] = !this.packageExpandedBlock[id];
  this.packageViewEnable[id] =true;
  this.setState({
  packageButton : this.packageExpandedBlock[id],
  viewEye:this.packageViewEnable[id]
  });
 }

 checkBoxHandleChange(id,item) {
    this.checked[id] = !this.checked[id]; 
    // console.log(this.checked[id],"-->checkbox");
    this.setState({checkclick : this.checked[id]});  
    this.allCheckClick = this.checked.filter(function(c) {
        return c;
    }).length === this.checked.length;
   //CheckBox Approve
    this.setState(previous => {
      let checkclickItem = previous.checkclickItem;
      let index = checkclickItem.indexOf(item) // check to see if the name is already stored in the array
      if (index === -1) {
        checkclickItem.push(item) // if it isn't stored add it to the array
      } else {
        checkclickItem.splice(index, 1) // if it is stored then remove it from the array
      }
      return { checkclickItem }; // save the new checkclickItem value in state
    });
 }
 PackageCheckBoxHandleChange(id,item) {
  this.packageChecked[id] = !this.packageChecked[id];
  this.setState({checkclick : this.packageChecked[id]});   
  this.allCheckClick = this.packageChecked.filter(function(c) {
      return c;
  }).length === this.packageChecked.length;
 //CheckBox Approve
  this.setState(previous => {
    let checkclickItem = previous.checkclickItem;
    let index = checkclickItem.indexOf(item) // check to see if the name is already stored in the array
    if (index === -1) {
      checkclickItem.push(item) // if it isn't stored add it to the array
    } else {
      checkclickItem.splice(index, 1) // if it is stored then remove it from the array
    }
    return { checkclickItem }; // save the new checkclickItem value in state
  });
}

onTriggerPress() {
  this.setState({ actionVisible: true });
}

onBackdropPress() {
  this.setState({ actionVisible: false });
}

 cardLongPress(id) {
  this.pressCard[id] = !this.pressCard[id];
  this.setState({longPress : this.pressCard[id]});
 }
 PackageCardLongPress(id) {
  this.packagePressCard[id] = !this.packagePressCard[id];
  this.setState({longPress : this.packagePressCard[id]});
 }
  openEditPopUp = (value,id,refRange) => {
  this.setState({editPopupvisible : true});
  this.setState(
    {
      editValue : value,
      editValueId : id,
      editReferenceRange : refRange
    });
 }
 openInfoPopUp = (value) => {
  this.setState({infoPopupVisable : true});
  this.setState({infoPopupValue : value});
 }
  openCommentPopUp = (value,id) => {
  this.setState({commentPopupVisable : true});
  this.setState(
    {
      medicalComment : value.MedicalRemarks,
      technicalComment:value.Reason,
      commentValueId : id,
    });
 }
  openActionSheet=(item,id) =>{
    this.invactioMENU[id] = !this.invactioMENU[id];
    this.setState({ actionVisible: true});
    this.setState({ actionValue: item});
    this.setState({
      recollectReson : item.Reason,
      recollectId : item.TestID,
      rerunReson : item.Reason,
      rerunId : item.TestID
    });
    this.setState({invactionPopUp : this.invactioMENU[id]});
    }
    grpOpenActionSheet=(item,id) =>{
      this.grpactioMENU[id] = !this.grpactioMENU[id];
      this.setState({ actionVisible: true});
      this.setState({ actionValue: item});
      this.setState({
        recollectReson : item.Reason,
        recollectId : item.TestID,
        rerunReson : item.Reason,
        rerunId : item.TestID
      });
      this.setState({grpactionPopUp : this.grpactioMENU[id]});
      }
  packageopenActionSheet=(item,id) =>{
    this.pakgrpactioMENU[id] = !this.pakgrpactioMENU[id];
    this.setState({ actionVisible: true});
    this.setState({ actionValue: item});
    this.setState({
      recollectReson : item.Reason,
      recollectId : item.TestID,
      rerunReson : item.Reason,
      rerunId : item.TestID
    });
    this.setState({pakGrpactionPopUp : this.pakgrpactioMENU[id]});
      }

  handleEditValue = (text) => {
  this.setState({ editValue: text })
 }
 updateEditValue = () =>{
  this.setState({editPopupvisible : false});
  
  for (let i = 0; i < this.state.investigationName.length; i++) { 
    if(this.state.investigationName[i].TestType == "GRP"){
       for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
         if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo==null){
          if (this.state.investigationName[i].OrderContentListInfo[j].TestID == this.state.editValueId) {
            this.state.investigationName[i].OrderContentListInfo[j].TestValue =this.state.editValue;
           } 
         }else if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo!=null){
          for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
            if (this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID == this.state.editValueId) {
              this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestValue =this.state.editValue;
              console.log(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestValue,"--value");
             } 
          }
         }
         
   }
    }else if(this.state.investigationName[i].TestType == "INV"){
      if (this.state.investigationName[i].TestID == this.state.editValueId) {
        this.state.investigationName[i].TestValue =this.state.editValue;
    }
    }else if(this.state.investigationName[i].TestType == "PKG"){
      for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
        if(this.state.investigationName[i].OrderContentListInfo[j].TestType=="GRP"){
          for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
            if (this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID == this.state.editValueId) {
              this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestValue =this.state.editValue;
             }  
          }
        }else if (this.state.investigationName[i].OrderContentListInfo[j].TestType=="INV") {
          if (this.state.investigationName[i].OrderContentListInfo[j].TestID == this.state.editValueId) {
            this.state.investigationName[i].OrderContentListInfo[j].TestValue =this.state.editValue;
           }
        }  
  }
    }
  } 
 }
 handleMedicalValue = (text) => {
  this.setState({ medicalComment: text })
 }

 handleTechnicalValue= (text) => {
  this.setState({ technicalComment: text })
 }
 handlereCollectValue= (item) => {
  this.setState({ recollectReson: item.label })
  console.log(this.state.recollectReson,"--->Recollect");
 }
 handlereRunValue= (item) => {
  this.setState({ rerunReson: item.label  })
  console.log(this.state.rerunReson,"--->rerun");
 }
 deltaDetails(item,name,vNum) {
  this.setState({ actionVisible: false });
  Actions.deltaValue(
    { 
      data: item,
      patientName:name,
      visitNumber:vNum}
    )
}
 //Service
 base_url = url.base_url;

 getInvesetigationValues= async () =>{
  const URL=await AsyncStorage.getItem('instanceURL');
  const sessionId=await AsyncStorage.getItem('SessionID');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const roleId=await AsyncStorage.getItem('roleId');
// console.log(locationId,"--->locationId");
fetch(
  URL + "API/InvestigationValues/GetPatientInvValues?LoginID="+loginId+"&OrgID="+orgId+"&VisitID="+this.state.patientInfo.PatientVisitID+"&RoleID="+roleId+"",
   { 
  method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      "ResWrapperReqYN":"Y",
      "LoginID":loginId,
      "SessionID":sessionId
      }
      }).then((response) => response.json())
        .then((responseJson) => { 
          // console.log(responseJson,"--->GetPatientInvValues");
        if (responseJson.status =="OK") {
          this.setState({ patientName: responseJson.response.PatientInfo.PatientName});
          this.setState({ visitNumber: responseJson.response.PatientVisitInfo.VisitNumber});
           this.setState({ investigationName: responseJson.response.TestDetailsList});
           console.log(this.state.investigationName,"--->responseJson");
           for (let i = 0; i < responseJson.response.TestDetailsList.length; i++) {
                this.expandedBlock[i]=false;
                this.checked[i]=false;
                this.pressCard[i]=false;
                this.viewEnable[i]=false;
                this.invactioMENU[i]=false;
                
                if(responseJson.response.TestDetailsList[i].OrderContentListInfo != null){
                  for (let j = 0; j < responseJson.response.TestDetailsList[i].OrderContentListInfo.length; j++) {
                       this.packageExpandedBlock[j]=false;
                       this.packageChecked[j]=false;
                       this.packagePressCard[j]=false;
                       this.packageViewEnable[j]=false;
                       this.grpactioMENU[j]=false;

                    if(responseJson.response.TestDetailsList[i].OrderContentListInfo[j].ParameterListInfo!= null){
                      for (let k = 0; k < responseJson.response.TestDetailsList[i].OrderContentListInfo[j].ParameterListInfo.length; k++) {
                        this.pakgrpactioMENU[k]=false;
                      }
                     }
                  }  
                }      
          }
        } else {
          this.sessionExpired(responseJson);
        }
        })
        .catch((error) => {
          console.error(error);
        });
} 


getReason= async () =>{
const URL=await AsyncStorage.getItem('instanceURL');
const sessionId=await AsyncStorage.getItem('SessionID');
const loginId=await AsyncStorage.getItem('LoginID');
const orgId=await AsyncStorage.getItem('orgId');
const roleId=await AsyncStorage.getItem('roleId');
// console.log(locationId,"--->locationId");
fetch(
URL + "API/InvestigationValues/RecollectReason?OrgID="+orgId+"",
  { 
method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    "ResWrapperReqYN":"Y",
    "LoginID":loginId,
    "SessionID":sessionId
    }
    }).then((response) => response.json())
      .then((responseJson) => { 
        //  console.log(responseJson,"--->Reason");
      if (responseJson.status =="OK") {
        this.setState({ reasonName: responseJson.response});
        let recollect_rerun = [];
        for (let i = 0; i < this.state.reasonName.length; i++) { 
          recollect_rerun.push(this.state.reasonName[i]);
            this.setState({ recollect_rerun: recollect_rerun});
        }
      }
      })
      .catch((error) => {
        console.error(error);
      });
} 
submitApproveAll =async () => {  
  const currentDate= moment(this.state.newDate).format('YYYYMMDD hh:mm:ss');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const locationId=await AsyncStorage.getItem('LocationId');
  const roleId=await AsyncStorage.getItem('roleId');
   var obj ;
   var  lstInv_RefPatientValues=[];
  // console.log(this.state.checkclickItem,"--->CheckedGRP");
  for (let c = 0; c < this.state.checkclickItem.length; c++) { 
    if(this.state.checkclickItem[c].TestType=="INV" && (this.state.checkclickItem[c].Reason==null||this.state.checkclickItem[c].Reason=="")){
      this.state.checkclickItem[c].TestStatus="Approve";
    }else if(this.state.checkclickItem[c].TestStatus=="Validate" && (this.state.checkclickItem[c].Reason!=null || this.state.checkclickItem[c].Reason!="") ){
      this.state.checkclickItem[c].TestStatus="Approve";
    }
    if(this.state.checkclickItem[c].TestType=="GRP"){
      if(this.state.checkclickItem[c].OrderContentListInfo!=null){
        for (let d = 0; d < this.state.checkclickItem[c].OrderContentListInfo.length; d++) {
          // this.reasonStatus[d]=null; 
          // console.log(this.reasonStatus[d],"--->reasoStatus")
          if(this.state.checkclickItem[c].OrderContentListInfo[d].Reason==null || this.state.checkclickItem[c].OrderContentListInfo[d].Reason==""){
            this.state.checkclickItem[c].OrderContentListInfo[d].TestStatus="Approve";
          }else if(this.state.checkclickItem[c].OrderContentListInfo[d].TestStatus=="Validate" && (this.state.checkclickItem[c].OrderContentListInfo[d].Reason!=null || this.state.checkclickItem[c].OrderContentListInfo[d].Reason!="") ){
            this.state.checkclickItem[c].OrderContentListInfo[d].TestStatus="Approve";
          }
        }
      }else if(this.state.checkclickItem[c].ParameterListInfo!=null){
        for (let e = 0; e < this.state.checkclickItem[c].ParameterListInfo.length; e++) { 
          if(this.state.checkclickItem[c].ParameterListInfo[e].Reason==null || this.state.checkclickItem[c].ParameterListInfo[e].Reason==""){
            this.state.checkclickItem[c].ParameterListInfo[e].TestStatus="Approve";
          }else if(this.state.checkclickItem[c].ParameterListInfo[e].TestStatus=="Validate" && (this.state.checkclickItem[c].ParameterListInfo[e].Reason!=null || this.state.checkclickItem[c].ParameterListInfo[e].Reason!="") ){
            this.state.checkclickItem[c].OrderContentListInfo[d].TestStatus="Approve";
          }
        }
      }
    }
  }
   for (let i = 0; i < this.state.investigationName.length; i++) { 
    //  1.Type=INV
    if(this.state.investigationName[i].TestType=="INV"){
      // for (let c = 0; c < this.state.checkclickItem.length; c++) { 
          lstInv_RefPatientValues.push(
            {
              "InvestigationName":this.state.investigationName[i].TestName,
              "InvestigationID":this.state.investigationName[i].TestID,
              "GroupID":this.state.investigationName[i].GroupID,
              "GroupName":"",
              "GroupComment":"",
              "PatientVisitID":this.state.investigationName[i].PatientVisitID,
              "CreatedBy":loginId,
              "CollectedDateTime":currentDate,
              "Status":this.state.investigationName[i].TestStatus,
              "ComplaintID":this.state.investigationName[i].ComplaintId,
              "Type":this.state.investigationName[i].Type,
              "OrgID":orgId,
              "InvestigationMethodID":this.state.investigationName[i].InvestigationMethodID,
              "MethodName":"",
              "KitID":this.state.investigationName[i].KitID,
              "KitName":"",
              "InstrumentID":this.state.investigationName[i].InstrumentID,
              "InstrumentName":"",
              "Interpretation":"",
              "PrincipleID":this.state.investigationName[i].PrincipleID,
              "PrincipleName":"",
              "QCData":"",
              "InvestigationSampleContainerID":this.state.investigationName[i].InvestigationSampleContainerID,
              "PackageID":this.state.investigationName[i].PackageID,
              "PackageName":"",
              "Reason":this.state.investigationName[i].Reason,
              "ReportStatus":"",
              "ReferenceRange":this.state.investigationName[i].ReferenceRange,
              "PerformingPhysicainName":"",
              "ApprovedBy":loginId,
              "GUID":this.state.patientInfo.GUID,
              "IsAbnormal":this.state.investigationName[i].IsAbnormal,
              "AccessionNumber":this.state.investigationName[i].AccessionNumber,
              "AutoApproveLoginID":this.state.investigationName[i].AutoApproveLoginID,
              "ValidatedBy":this.state.investigationName[i].ValidatedBy,
              "RemarksID":this.state.investigationName[i].RemarksID,
              "MedicalRemarks":this.state.investigationName[i].MedicalRemarks,
              "GroupMedicalRemarks":"",
              "InvSampleStatusID":this.state.investigationName[i].InvSampleStatusID,
              "AuthorizedBy":this.state.investigationName[i].AuthorizedBy,
              "ConvReferenceRange":"",
              "ManualAbnormal":this.state.investigationName[i].ManualAbnormal,
              "IsAutoAuthorize":this.state.investigationName[i].IsAutoAuthorize,
              "PrintableRange":"",
              "IsAutoValidate":"",
              "InvStatusReasonID":this.state.investigationName[i].InvStatusReasonID,
              "IsSensitive":this.state.investigationName[i].IsSensitive,
              "IsReportable":this.state.investigationName[i].IsReportable,
              "Value":this.state.investigationName[i].TestValue,
              "Dilution":"",
              "DeviceID":"",
              "DeviceValue":"",
              "DeviceActualValue":"" ,
              "ConvValue":"",
              "ConvUOMCode":this.state.investigationName[i].UOMCode
            });
      // }       
    }
    
    // 2.Type=GRP

    if(this.state.investigationName[i].TestType=="GRP"){
      for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
          if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo==null){
              lstInv_RefPatientValues.push(
                {
                  "InvestigationName":this.state.investigationName[i].OrderContentListInfo[j].TestName,
                  "InvestigationID":this.state.investigationName[i].OrderContentListInfo[j].TestID,
                  "GroupID":this.state.investigationName[i].OrderContentListInfo[j].OrgGroupID,
                  "GroupName":this.state.investigationName[i].OrderContentListInfo[j].GroupName,
                  "GroupComment":"",
                  "PatientVisitID":this.state.investigationName[i].PatientVisitID,
                  "CreatedBy":loginId,
                  "CollectedDateTime":currentDate,
                  "Status":this.state.investigationName[i].OrderContentListInfo[j].TestStatus,
                  "ComplaintID":this.state.investigationName[i].OrderContentListInfo[j].ComplaintId,
                  "Type":this.state.investigationName[i].OrderContentListInfo[j].Type,
                  "OrgID":orgId,
                  "InvestigationMethodID":this.state.investigationName[i].OrderContentListInfo[j].InvestigationMethodID,
                  "MethodName":"",
                  "KitID":this.state.investigationName[i].OrderContentListInfo[j].KitID,
                  "KitName":"",
                  "InstrumentID":this.state.investigationName[i].OrderContentListInfo[j].InstrumentID,
                  "InstrumentName":"",
                  "Interpretation":"",
                  "PrincipleID":this.state.investigationName[i].OrderContentListInfo[j].PrincipleID,
                  "PrincipleName":"",
                  "QCData":"",
                  "InvestigationSampleContainerID":this.state.investigationName[i].OrderContentListInfo[j].InvestigationSampleContainerID,
                  "PackageID":this.state.investigationName[i].OrderContentListInfo[j].PackageID,
                  "PackageName":"",
                  "Reason":this.state.investigationName[i].OrderContentListInfo[j].Reason,
                  "ReportStatus":"",
                  "ReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ReferenceRange,
                  "PerformingPhysicainName":"",
                  "ApprovedBy":loginId,
                  "GUID":this.state.patientInfo.GUID,
                  "IsAbnormal":this.state.investigationName[i].OrderContentListInfo[j].IsAbnormal,
                  "AccessionNumber":this.state.investigationName[i].OrderContentListInfo[j].AccessionNumber,
                  "AutoApproveLoginID":this.state.investigationName[i].OrderContentListInfo[j].AutoApproveLoginID,
                  "ValidatedBy":this.state.investigationName[i].OrderContentListInfo[j].ValidatedBy,
                  "RemarksID":this.state.investigationName[i].OrderContentListInfo[j].RemarksID,
                  "MedicalRemarks":this.state.investigationName[i].OrderContentListInfo[j].MedicalRemarks,
                  "GroupMedicalRemarks":"",
                  "InvSampleStatusID":this.state.investigationName[i].OrderContentListInfo[j].InvSampleStatusID,
                  "AuthorizedBy":this.state.investigationName[i].OrderContentListInfo[j].AuthorizedBy,
                  "ConvReferenceRange":"",
                  "ManualAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ManualAbnormal,
                  "IsAutoAuthorize":this.state.investigationName[i].OrderContentListInfo[j].IsAutoAuthorize,
                  "PrintableRange":"",
                  "IsAutoValidate":"",
                  "InvStatusReasonID":this.state.investigationName[i].OrderContentListInfo[j].InvStatusReasonID,
                  "IsSensitive":this.state.investigationName[i].OrderContentListInfo[j].IsSensitive,
                  "IsReportable":this.state.investigationName[i].OrderContentListInfo[j].IsReportable,
                  "Value":this.state.investigationName[i].OrderContentListInfo[j].TestValue,
                  "Dilution":"",
                  "DeviceID":"",
                  "DeviceValue":"",
                  "DeviceActualValue":"" ,
                  "ConvValue":"",
                  "ConvUOMCode":this.state.investigationName[i].OrderContentListInfo[j].UOMCode
                });
          }else if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo!=null){
            for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
                  lstInv_RefPatientValues.push(
                    {
                      "InvestigationName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestName,
                      "InvestigationID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID,
                      "GroupID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].OrgGroupID,
                      "GroupName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].SubGroupName,
                      "GroupComment":"",
                      "PatientVisitID":this.state.investigationName[i].PatientVisitID,
                      "CreatedBy":loginId,
                      "CollectedDateTime":currentDate,
                      "Status":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestStatus,
                      "ComplaintID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ComplaintId,
                      "Type":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Type,
                      "OrgID":orgId,
                      "InvestigationMethodID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvestigationMethodID,
                      "MethodName":"",
                      "KitID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].KitID,
                      "KitName":"",
                      "InstrumentID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InstrumentID,
                      "InstrumentName":"",
                      "Interpretation":"",
                      "PrincipleID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PrincipleID,
                      "PrincipleName":"",
                      "QCData":"",
                      "InvestigationSampleContainerID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvestigationSampleContainerID,
                      "PackageID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PackageID,
                      "PackageName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PackageName,
                      "Reason":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason,
                      "ReportStatus":"",
                      "ReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ReferenceRange,
                      "PerformingPhysicainName":"",
                      "ApprovedBy":loginId,
                      "GUID":this.state.patientInfo.GUID,
                      "IsAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsAbnormal,
                      "AccessionNumber":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AccessionNumber,
                      "AutoApproveLoginID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AutoApproveLoginID,
                      "ValidatedBy":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ValidatedBy,
                      "RemarksID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].RemarksID,
                      "MedicalRemarks":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].MedicalRemarks,
                      "GroupMedicalRemarks":"",
                      "InvSampleStatusID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvSampleStatusID,
                      "AuthorizedBy":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AuthorizedBy,
                      "ConvReferenceRange":"",
                      "ManualAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ManualAbnormal,
                      "IsAutoAuthorize":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsAutoAuthorize,
                      "PrintableRange":"",
                      "IsAutoValidate":"",
                      "InvStatusReasonID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvStatusReasonID,
                      "IsSensitive":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsSensitive,
                      "IsReportable":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsReportable,
                      "Value":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestValue,
                      "Dilution":"",
                      "DeviceID":"",
                      "DeviceValue":"",
                      "DeviceActualValue":"" ,
                      "ConvValue":"",
                      "ConvUOMCode":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].UOMCode
                    });
            }
          } 
        
      }
    }
   
    // 3.Type=PKG

    if(this.state.investigationName[i].TestType=="PKG"){
      for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
        if(this.state.investigationName[i].OrderContentListInfo[j].TestType=="INV"){
          //for (let c = 0; c < this.state.checkclickItem.length; c++) {
              lstInv_RefPatientValues.push(
                {
                  "InvestigationName":this.state.investigationName[i].OrderContentListInfo[j].TestName,
                  "InvestigationID":this.state.investigationName[i].OrderContentListInfo[j].TestID,
                  "GroupID":this.state.investigationName[i].OrderContentListInfo[j].GroupID,
                  "GroupName":this.state.investigationName[i].OrderContentListInfo[j].GroupName,
                  "GroupComment":"",
                  "PatientVisitID":this.state.investigationName[i].PatientVisitID,
                  "CreatedBy":loginId,
                  "CollectedDateTime":currentDate,
                  "Status":this.state.investigationName[i].OrderContentListInfo[j].TestStatus,
                  "ComplaintID":this.state.investigationName[i].OrderContentListInfo[j].ComplaintId,
                  "Type":this.state.investigationName[i].OrderContentListInfo[j].Type,
                  "OrgID":orgId,
                  "InvestigationMethodID":this.state.investigationName[i].OrderContentListInfo[j].InvestigationMethodID,
                  "MethodName":"",
                  "KitID":this.state.investigationName[i].OrderContentListInfo[j].KitID,
                  "KitName":"",
                  "InstrumentID":this.state.investigationName[i].OrderContentListInfo[j].InstrumentID,
                  "InstrumentName":"",
                  "Interpretation":"",
                  "PrincipleID":this.state.investigationName[i].OrderContentListInfo[j].PrincipleID,
                  "PrincipleName":"",
                  "QCData":"",
                  "InvestigationSampleContainerID":this.state.investigationName[i].OrderContentListInfo[j].InvestigationSampleContainerID,
                  "PackageID":this.state.investigationName[i].OrderContentListInfo[j].PackageID,
                  "PackageName":this.state.investigationName[i].OrderContentListInfo[j].PackageName,
                  "Reason":this.state.investigationName[i].OrderContentListInfo[j].Reason,
                  "ReportStatus":"",
                  "ReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ReferenceRange,
                  "PerformingPhysicainName":"",
                  "ApprovedBy":loginId,
                  "GUID":this.state.patientInfo.GUID,
                  "IsAbnormal":this.state.investigationName[i].OrderContentListInfo[j].IsAbnormal,
                  "AccessionNumber":this.state.investigationName[i].OrderContentListInfo[j].AccessionNumber,
                  "AutoApproveLoginID":this.state.investigationName[i].OrderContentListInfo[j].AutoApproveLoginID,
                  "ValidatedBy":this.state.investigationName[i].OrderContentListInfo[j].ValidatedBy,
                  "RemarksID":this.state.investigationName[i].OrderContentListInfo[j].RemarksID,
                  "MedicalRemarks":this.state.investigationName[i].OrderContentListInfo[j].MedicalRemarks,
                  "GroupMedicalRemarks":"",
                  "InvSampleStatusID":this.state.investigationName[i].OrderContentListInfo[j].InvSampleStatusID,
                  "AuthorizedBy":this.state.investigationName[i].OrderContentListInfo[j].AuthorizedBy,
                  "ConvReferenceRange":"",
                  "ManualAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ManualAbnormal,
                  "IsAutoAuthorize":this.state.investigationName[i].OrderContentListInfo[j].IsAutoAuthorize,
                  "PrintableRange":"",
                  "IsAutoValidate":"",
                  "InvStatusReasonID":this.state.investigationName[i].OrderContentListInfo[j].InvStatusReasonID,
                  "IsSensitive":this.state.investigationName[i].OrderContentListInfo[j].IsSensitive,
                  "IsReportable":this.state.investigationName[i].OrderContentListInfo[j].IsReportable,
                  "Value":this.state.investigationName[i].OrderContentListInfo[j].TestValue,
                  "Dilution":"",
                  "DeviceID":"",
                  "DeviceValue":"",
                  "DeviceActualValue":"" ,
                  "ConvValue":"",
                  "ConvUOMCode":this.state.investigationName[i].OrderContentListInfo[j].UOMCode
                  
                });
       // }
      }else if(this.state.investigationName[i].OrderContentListInfo[j].TestType=="GRP" &&this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo!=null ){
      for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
       // for (let c = 0; c < this.state.checkclickItem.length; c++) {
            lstInv_RefPatientValues.push(
              {
                "InvestigationName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestName,
                "InvestigationID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID,
                "GroupID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].OrgGroupID,
                "GroupName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].SubGroupID != 0?
                this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].SubGroupName:
                this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].GroupName,
                "GroupComment":"",
                "PatientVisitID":this.state.investigationName[i].PatientVisitID,
                "CreatedBy":loginId,
                "CollectedDateTime":currentDate,
                "Status":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestStatus,
                "ComplaintID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ComplaintId,
                "Type":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Type,
                "OrgID":orgId,
                "InvestigationMethodID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvestigationMethodID,
                "MethodName":"",
                "KitID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].KitID,
                "KitName":"",
                "InstrumentID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InstrumentID,
                "InstrumentName":"",
                "Interpretation":"",
                "PrincipleID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PrincipleID,
                "PrincipleName":"",
                "QCData":"",
                "InvestigationSampleContainerID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvestigationSampleContainerID,
                "PackageID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PackageID,
                "PackageName":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].PackageName,
                "Reason":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason,
                "ReportStatus":"",
                "ReferenceRange":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ReferenceRange,
                "PerformingPhysicainName":"",
                "ApprovedBy":loginId,
                "GUID":this.state.patientInfo.GUID,
                "IsAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsAbnormal,
                "AccessionNumber":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AccessionNumber,
                "AutoApproveLoginID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AutoApproveLoginID,
                "ValidatedBy":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ValidatedBy,
                "RemarksID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].RemarksID,
                "MedicalRemarks":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].MedicalRemarks,
                "GroupMedicalRemarks":"",
                "InvSampleStatusID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvSampleStatusID,
                "AuthorizedBy":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].AuthorizedBy,
                "ConvReferenceRange":"",
                "ManualAbnormal":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].ManualAbnormal,
                "IsAutoAuthorize":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsAutoAuthorize,
                "PrintableRange":"",
                "IsAutoValidate":"",
                "InvStatusReasonID":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].InvStatusReasonID,
                "IsSensitive":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsSensitive,
                "IsReportable":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].IsReportable,
                "Value":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestValue,
                "Dilution":"",
                "DeviceID":"",
                "DeviceValue":"",
                "DeviceActualValue":"" ,
                "ConvValue":"",
                "ConvUOMCode":this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].UOMCode
              });
        //}
      }
    }
  }
  }
  }
   
    obj=({
      "ApprovadBy":loginId,
      "LocationID":locationId,
      "OrgID":orgId,
      "PatientID":this.state.patientInfo.PatientID,
      "RoleID":roleId,
      "TaskID":this.state.patientInfo.TaskID,
      "VisitID":this.state.patientInfo.PatientVisitID,
      "deptID":0,
      "gUID":this.state.patientInfo.GUID,
  
      "lstInvestigationValues":lstInv_RefPatientValues,
      "lstReflexPatientinvestigation":lstInv_RefPatientValues
   })
    if(this.state.checkclickItem.length==0){
      this.showToast("Please long press the Test and select the checkbox to Approve");
    }else if(this.state.checkclickItem.length != 0){
      this.Investigation(obj);
    }

}

Investigation =async (obj) => {
  const URL=await AsyncStorage.getItem('instanceURL');
  const sessionId=await AsyncStorage.getItem('SessionID');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const locationId=await AsyncStorage.getItem('LocationId');
  const roleId=await AsyncStorage.getItem('roleId');
  console.log(obj,"--->GRP");
  fetch(URL + "API/InvestigationValues/SaveInvestigationResults", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "ResWrapperReqYN":"Y",
        "LoginID":loginId,
        "SessionID":sessionId
        },
        body: JSON.stringify(obj)
  }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson,"--->SaveInv");
        if (responseJson.status =="OK" && responseJson.response =="Success") {
        this.showToast("Approved Successfully");
        Actions.dashboard();
        }else {
          this.sessionExpired(responseJson);
        }
      })
      .catch((error) => {
          console.error(error);
      });
}


showToast = (message) => {
  Toast.show({
    text: message,
    buttonText: "Okay",
    duration: 3000
  })
}
sessionExpired = (responseJson) => {
  if (responseJson.status == "InternalServerError" ||
  responseJson.Message == "An error has occurred.") {
      AsyncStorage.clear();
      Actions.login();
      if (responseJson.status == "InternalServerError")
       {
        this.showToast("Session Expired");
      }else if(responseJson.Message == "An error has occurred.")
      {
        this.showToast("Session Expired");
      }
      
    
  }
}

//Design
  render() {
  var  reasonDropDownitems=[];
  for (let i = 0; i < this.state.recollect_rerun.length; i++) { 
    reasonDropDownitems.push({label: this.state.recollect_rerun[i].Reason, value: this.state.recollect_rerun[i].ReasonID});
  }
 
  var childCount;
    // for (let i = 0; i < this.state.investigationName.length; i++) { 
    //   if(this.state.investigationName[i].GroupID != 0){
    //      for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
    //        for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
    //         childCount = this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; 
    //      }
    //     }
    //   }    
    //  } 
    const handleCancel = () => {
      this.setState({editPopupvisible : false});
    };
  
    const UpdateRecollect = () => {
      this.setState({recollectVisable : false});
      for (let i = 0; i < this.state.investigationName.length; i++) { 
        if(this.state.investigationName[i].TestType == "GRP"){
          // if (this.state.investigationName[i].TestID == this.state.recollectId) {
          //   this.state.investigationName[i].Reason =this.state.recollectReson;
          //   this.state.investigationName[i].TestStatus ="Retest";
          // //  console.log(this.state.investigationName[i].OrderContentListInfo[j].TestStatus,"--TestStatus");
          //  } 
           for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
            if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo==null){
              if (this.state.investigationName[i].OrderContentListInfo[j].TestID == this.state.recollectId) {
                this.state.investigationName[i].OrderContentListInfo[j].Reason =this.state.recollectReson;
                this.state.investigationName[i].OrderContentListInfo[j].TestStatus ="Retest";
              //  console.log(this.state.investigationName[i].OrderContentListInfo[j].TestStatus,"--TestStatus");
               } 
             }else if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo!=null){
              for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
                if (this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID == this.state.recollectId) {
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason =this.state.recollectReson;
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestStatus ="Retest"; 
                  // console.log(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestStatus,"--TestStatus");
                 } 
              }
             } 
       }
        }else if(this.state.investigationName[i].TestType == "INV"){
          if (this.state.investigationName[i].TestID == this.state.recollectId) {
            this.state.investigationName[i].Reason =this.state.recollectReson;
            this.state.investigationName[i].TestStatus ="Retest";
        }
        }else if(this.state.investigationName[i].TestType == "PKG"){
          for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
            if(this.state.investigationName[i].OrderContentListInfo[j].TestType=="GRP"){
              for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
                if (this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID == this.state.recollectId) {
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason =this.state.recollectReson;
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestStatus ="Retest"
                 }  
              }
            }else if (this.state.investigationName[i].OrderContentListInfo[j].TestType=="INV") {
              if (this.state.investigationName[i].OrderContentListInfo[j].TestID == this.state.recollectId) {
                this.state.investigationName[i].OrderContentListInfo[j].Reason =this.state.recollectReson;
                this.state.investigationName[i].OrderContentListInfo[j].TestStatus ="Retest"
               }
            }  
      }
        }
      } 
    };

    const UpdateRun = () => {
      this.setState({rerunVisable : false});
      for (let i = 0; i < this.state.investigationName.length; i++) { 
        if(this.state.investigationName[i].TestType == "GRP"){
           for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
            if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo==null){
              if (this.state.investigationName[i].OrderContentListInfo[j].TestID == this.state.rerunId) {
                this.state.investigationName[i].OrderContentListInfo[j].Reason =this.state.rerunReson;
                this.state.investigationName[i].OrderContentListInfo[j].TestStatus ="Recheck";
               } 
             }else if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo!=null){
              for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
                if (this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID == this.state.rerunId) {
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason =this.state.rerunReson;
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestStatus ="Recheck"; 
                  // console.log(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestValue,"--value");
                 } 
              }
             } 
       }
        }else if(this.state.investigationName[i].TestType == "INV"){
          if (this.state.investigationName[i].TestID == this.state.rerunId) {
            this.state.investigationName[i].Reason =this.state.rerunReson;
            this.state.investigationName[i].TestStatus ="Recheck";
        }
        }else if(this.state.investigationName[i].TestType == "PKG"){
          for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
            if(this.state.investigationName[i].OrderContentListInfo[j].TestType=="GRP"){
              for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
                if (this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID == this.state.rerunId) {
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason =this.state.rerunReson;
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestStatus ="Recheck"
                 }  
              }
            }else if (this.state.investigationName[i].OrderContentListInfo[j].TestType=="INV") {
              if (this.state.investigationName[i].OrderContentListInfo[j].TestID == this.state.rerunId) {
                this.state.investigationName[i].OrderContentListInfo[j].Reason =this.state.rerunReson;
                this.state.investigationName[i].OrderContentListInfo[j].TestStatus ="Recheck"
               }
            }  
      }
        }
      } 
    };
  
    const handleCommentCancel = () => {
      this.setState({commentPopupVisable : false});
    };
  
    const handleAddComment = () => {
      this.setState({commentPopupVisable : false});

      for (let i = 0; i < this.state.investigationName.length; i++) { 
        if(this.state.investigationName[i].TestType == "GRP"){
           for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
            if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo==null){
              if (this.state.investigationName[i].OrderContentListInfo[j].TestID == this.state.commentValueId) {
                this.state.investigationName[i].OrderContentListInfo[j].MedicalRemarks =this.state.medicalComment;
                this.state.investigationName[i].OrderContentListInfo[j].Reason =this.state.technicalComment;
                this.state.investigationName[i].OrderContentListInfo[j].TestStatus ="Approve";
               } 
             }else if(this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo!=null){
              for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
                if (this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID == this.state.commentValueId) {
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].MedicalRemarks =this.state.medicalComment;
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason =this.state.technicalComment;
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestStatus ="Approve";
                } 
              }
             }
       }
        }else if(this.state.investigationName[i].TestType == "INV"){
          if (this.state.investigationName[i].TestID == this.state.commentValueId) {
            this.state.investigationName[i].MedicalRemarks =this.state.medicalComment;
            this.state.investigationName[i].Reason =this.state.technicalComment;
            this.state.investigationName[i].TestStatus ="Approve";
        }
        }else if(this.state.investigationName[i].TestType == "PKG"){
          for (let j = 0; j < this.state.investigationName[i].OrderContentListInfo.length; j++) { 
            if(this.state.investigationName[i].OrderContentListInfo[j].TestType=="GRP"){
              for (let k = 0; k < this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo.length; k++) { 
                if (this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestID == this.state.commentValueId) {
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].MedicalRemarks =this.state.medicalComment;
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].Reason =this.state.technicalComment;
                  this.state.investigationName[i].OrderContentListInfo[j].ParameterListInfo[k].TestStatus ="Approve";
                }  
              }
            }else if (this.state.investigationName[i].OrderContentListInfo[j].TestType=="INV") {
              if (this.state.investigationName[i].OrderContentListInfo[j].TestID == this.state.commentValueId) {
                this.state.investigationName[i].OrderContentListInfo[j].MedicalRemarks =this.state.medicalComment;
                this.state.investigationName[i].OrderContentListInfo[j].Reason =this.state.technicalComment;
                this.state.investigationName[i].OrderContentListInfo[j].TestStatus ="Approve";
               }
            }  
      }
        }
      } 
    };
    
    const medicalexpanded=()=>{
      this.setState({medicalExpand : !this.state.medicalExpand});
     }
    const techincalexpanded=()=>{
      this.setState({techincalExpand : !this.state.techincalExpand});
     }
   
     const handleInfoCancel = () => {
      this.setState({infoPopupVisable : false});
     }  

    const openRecollect=() =>{
     this.setState({ recollectVisable: true });
     this.setState({ actionVisible: false });
    }
    const  openRerun=() =>{
     this.setState({ rerunVisable: true });
     this.setState({ actionVisible: false });
    }
    const  closeReCollect=() =>{
      this.setState({ recollectVisable: false });
      }
      const  closeReRun=() =>{
        this.setState({ rerunVisable: false });
        }
    return (
      <SafeAreaView  style={{flex:1}}>
      <Container>
         <Header style={{ backgroundColor:"#7387eb"}}>
        <Left>
        <Icon onPress={() => Actions.pop()} style={{ color:"#fff"}} name="arrow-back"/>
        </Left>
        <View>
        <Body>
        {Platform.OS === 'android'?
        <Title style={{ fontSize:15}}>{this.state.patientName}</Title>:
        Platform.OS === 'ios'?
        <Title style={{ fontSize:15,color:"#FFF"}}>{this.state.patientName}</Title>:null}
         <Subtitle style={styles.visitId}>{this.state.visitNumber}</Subtitle>
        </Body>
        </View>
        <Right >
         <Icon onPress={() => Actions.dashboard()} 
               style={{ color:"#fff"}} 
               name="home-outline"
         />
        </Right>
        </Header>
        <Content>
         <FlatList
          data={this.state.investigationName}
          renderItem={({ item, index}) => (
         <View>
           {item.TestType == "GRP"?
            <Card transparent style={styles.cardPatientInv}>
              {this.viewEnable[index]==true?
               <TouchableOpacity key={index}  activeOpacity={0.6} onLongPress={this.cardLongPress.bind(this, index)}>
            <CardItem header bordered 
            style={styles.cardHeader}>
                  <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>
                  <Text style={{ color:"#000",fontSize:14,fontWeight:"bold",paddingLeft:10}}>{item.TestName}</Text>
                      <Text style={{ color:"#bdb9b9",fontSize:13,paddingLeft:10,textAlign:"center"}}>Group</Text>
                  </Left>
                  <View>
                  <Right style={{ flexDirection:"row",justifyContent:"center"}} >
                  {this.pressCard[index]  ?
                  <CheckBox
                   checked={this.checked[index]}
                   onPress={this.checkBoxHandleChange.bind(this,index,item)}
                      />: null}
                  <TouchableOpacity key={index} style={styles.row} onPress={this.expanded.bind(this, index)}>     
                  <Image source={this.expandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                  style={{height:26,width:26,marginBottom:15}}/> 
                  </TouchableOpacity>
                  {this.viewEnable[index]==false || this.viewEnable==false?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                  <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                   {/* <TouchableOpacity style={{padding:20}} onPress={this.openActionSheet.bind(this,item,index)}>
                          <Image 
                           source={require("../image/more.png")}style={{height:7,width:32,}}/>
                        </TouchableOpacity>  */}
                 </Right>
                 </View>
                     {/* {this.invactioMENU[index]?
                         <MenuProvider>
                             <Menu
                               opened={this.state.actionVisible}>
                               <MenuTrigger
                                 onPress={() => this.onTriggerPress()}>
                             </MenuTrigger>
                               <MenuOptions>
                                 <MenuOption onSelect={openRecollect} text='ReCollect' />
                                 <MenuOption onSelect={openRerun}text='ReRun' />
                               </MenuOptions>
                             </Menu>
                           </MenuProvider>:null} */}
                </CardItem>
                </TouchableOpacity>:
                  <TouchableOpacity key={index}  activeOpacity={0.6}>
                  <CardItem header bordered 
                   style={styles.cardHeader}>
                    <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>
                    <Text style={{ color:"#000",fontSize:14,fontWeight:"bold",paddingLeft:10}}>{item.TestName}</Text>
                      <Text style={{ color:"#bdb9b9",fontSize:13,paddingLeft:10,textAlign:"center"}}>Group</Text>
                    </Left>
                    <View>
                   <Right style={{ flexDirection:"row",justifyContent:"center"}} >
                    {this.pressCard[index]  ?
                    <CheckBox
                      checked={this.checked[index]}
                      onPress={this.checkBoxHandleChange.bind(this,index,item)}
                        />: null}
                    <TouchableOpacity key={index} style={styles.row} onPress={this.expanded.bind(this, index)}>     
                    <Image source={this.expandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                    style={{height:26,width:26,marginBottom:15}}/> 
                    </TouchableOpacity>
                    {this.viewEnable[index]==false || this.viewEnable==false ?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                    <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                    </Right>
                    </View>

                   
                  </CardItem>
                  </TouchableOpacity>}
                {this.expandedBlock[index]  ?
                 <FlatList
                   data={item.OrderContentListInfo}
                  renderItem={({ item,index }) => (
                    <View>
                      {item.ParameterListInfo!=null?
                      <View>
                        {this.packageViewEnable[index]==true?
                      <TouchableOpacity key={index}  activeOpacity={0.6} onLongPress={this.PackageCardLongPress.bind(this, index)}>
                     <CardItem header bordered 
                     style={styles.cardHeader}>
                    <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>
                    <Text style={{ color:"#000",fontSize:14,fontWeight:"bold",paddingLeft:10}}>{item.TestName}</Text>
                      <Text style={{ color:"#bdb9b9",fontSize:13,paddingLeft:10,textAlign:"center"}}>Sub Group</Text>
                    </Left>
                    <View>
                    <Right style={{ flexDirection:"row",justifyContent:"center"}} >
                    {this.packagePressCard[index]  ?
                    <CheckBox
                    checked={this.packageChecked[index]}
                    onPress={this.PackageCheckBoxHandleChange.bind(this,index,item)}
                        />: null}
                    <TouchableOpacity key={index} style={styles.row} onPress={this.packageExpanded.bind(this, index)}>     
                    <Image source={this.packageExpandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                    style={{height:26,width:26,marginBottom:15}}/> 
                    </TouchableOpacity>
                    {this.packageViewEnable[index]==false || this.packageViewEnable==false?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                    <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                  </Right>
                    </View>
                  </CardItem>
                  </TouchableOpacity>:
                  <TouchableOpacity key={index}  activeOpacity={0.6}>
                  <CardItem header bordered 
                   style={styles.cardHeader}>
                    <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>
                        <Text style={{ color:"#000",fontSize:14,fontWeight:"bold",paddingLeft:10}}>{item.TestName}</Text>
                      <Text style={{ color:"#bdb9b9",fontSize:13,paddingLeft:10,textAlign:"center"}}>Sub Group</Text>
                    </Left>
                    <View>
                    <Right style={{ flexDirection:"row",justifyContent:"center"}} >
                    {this.pressCard[index]  ?
                    <CheckBox
                      checked={this.packageChecked[index]}
                      onPress={this.PackageCheckBoxHandleChange.bind(this,index,item)}
                        />: null}
                    <TouchableOpacity key={index} style={styles.row} onPress={this.packageExpanded.bind(this, index)}>     
                    <Image source={this.packageExpandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                    style={{height:26,width:26,marginBottom:15}}/> 
                    </TouchableOpacity>
                    {this.packageViewEnable[index]==false || this.viewEnable==false ?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                    <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                    </Right>
                    </View>
                    
                  </CardItem>
                  </TouchableOpacity>}
                        {this.packageExpandedBlock[index]  ?
                         <FlatList
                        data={item.ParameterListInfo}
                        renderItem={({ item,index }) => (
                         <CardItem style={styles.cardBody}>
                      <View style={{backgroundColor:"#fff", flex: 1,alignSelf:"center",width:350}}>
                        <View style={{flexDirection:"row",padding:10}}>
                        <Left>
                        <Text style={{margin:5}}>{item.TestName}</Text>
                        </Left>
                        <Right style={{padding:20}}>
                         <TouchableOpacity onPress={this.grpOpenActionSheet.bind(this,item,index)}>
                          <Image 
                           source={require("../image/more.png")}style={{height:7,width:32}}/>
                        </TouchableOpacity> 
                        {this.grpactioMENU[index]?
                         <MenuProvider style={{flex:1, flexDirection: 'column', padding: 50}}>
                             <Menu
                               opened={this.state.actionVisible}>
                               <MenuTrigger
                                 onPress={() => this.onTriggerPress()}>
                             </MenuTrigger>
                               <MenuOptions>
                                 <MenuOption onSelect={openRecollect} text='ReCollect' />
                                 <MenuOption onSelect={openRerun}text='ReRun' />
                                 <MenuOption onSelect={this.deltaDetails.bind(this, this.state.actionValue,this.state.patientName,this.state.visitNumber)} text='Delta' />
                               </MenuOptions>
                             </Menu>
                           </MenuProvider>:null}
                       </Right>
                       </View >
                       <View style={{flexDirection:"row",padding:10}}>
                       <Left>
                       <TouchableOpacity   onPress={this.openEditPopUp.bind(this,item.TestValue,item.TestID,item.ReferenceRange)}>
                       <Image 
                        source={require("../image/edit.png")}style={{height:20,width:20,margin:5}}/>
                       </TouchableOpacity>
                       {item.ReferenceColor=="LightGreen"?
                        <Text style={{fontWeight:"bold",color:"#42f5f2"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Red"?
                        <Text style={{fontWeight:"bold",color:"red"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="lightpink"?
                        <Text style={{fontWeight:"bold",color:"#ebabeb"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Yellow"?
                        <Text style={{fontWeight:"bold",color:"#e5ed6d"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Alert"?
                        <Text style={{fontWeight:"bold",color:"#d1d1d1"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Orange"?
                        <Text style={{fontWeight:"bold",color:"#f59e42"}}>{item.TestValue} {item.UOMCode}</Text>:
                        <Text style={{fontWeight:"bold"}}>{item.TestValue} {item.UOMCode}</Text>}
                       </Left>
                       <Right style={{flexDirection:"row",justifyContent:"space-around"}}>
                       <TouchableOpacity onPress={this.openInfoPopUp.bind(this,item)}>
                       <Image 
                        source={require("../image/location.png")}style={{height:30,width:30}}/>
                       </TouchableOpacity>
                        <TouchableOpacity onPress={this.openCommentPopUp.bind(this,item,item.TestID)}>
                        <Image 
                        source={require("../image/comment.png")}style={{height:30,width:30}}/>
                        </TouchableOpacity>
                       </Right>  
                       
                       </View>
                     </View>       
                     </CardItem>
                     )}
                     keyExtractor={(item, index) => index.toString()}
                      />:null}
                        </View>
                     :
                     <CardItem style={styles.cardBody}>
                     <View style={{backgroundColor:"#fff", flex: 1,alignSelf:"center",width:350}}>
                       <View style={{flexDirection:"row",padding:10}}>
                       <Left>
                       <Text style={{margin:5}}>{item.TestName}</Text>
                       </Left>
                       <Right style={{padding:20}}>
                        <TouchableOpacity onPress={this.grpOpenActionSheet.bind(this,item,index)}>
                         <Image 
                          source={require("../image/more.png")}style={{height:7,width:32}}/>
                       </TouchableOpacity> 
                       {this.grpactioMENU[index]?
                        <MenuProvider style={{flex:1, flexDirection: 'column', padding: 50}}>
                            <Menu
                              opened={this.state.actionVisible}>
                              <MenuTrigger
                                onPress={() => this.onTriggerPress()}>
                            </MenuTrigger>
                              <MenuOptions>
                                <MenuOption onSelect={openRecollect} text='ReCollect' />
                                <MenuOption onSelect={openRerun}text='ReRun' />
                                <MenuOption onSelect={this.deltaDetails.bind(this, this.state.actionValue,this.state.patientName,this.state.visitNumber)} text='Delta' />
                              </MenuOptions>
                            </Menu>
                          </MenuProvider>:null}
                      </Right>
                      </View >
                      <View style={{flexDirection:"row",padding:10}}>
                      <Left>
                      <TouchableOpacity   onPress={this.openEditPopUp.bind(this,item.TestValue,item.TestID,item.ReferenceRange)}>
                      <Image 
                       source={require("../image/edit.png")}style={{height:20,width:20,margin:5}}/>
                      </TouchableOpacity>
                      {item.ReferenceColor=="LightGreen"?
                        <Text style={{fontWeight:"bold",color:"#42f5f2"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Red"?
                        <Text style={{fontWeight:"bold",color:"red"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="lightpink"?
                        <Text style={{fontWeight:"bold",color:"#ebabeb"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Yellow"?
                        <Text style={{fontWeight:"bold",color:"#e5ed6d"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Alert"?
                        <Text style={{fontWeight:"bold",color:"#d1d1d1"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Orange"?
                        <Text style={{fontWeight:"bold",color:"#f59e42"}}>{item.TestValue} {item.UOMCode}</Text>:
                        <Text style={{fontWeight:"bold"}}>{item.TestValue} {item.UOMCode}</Text>}
                      </Left>
                      <Right style={{flexDirection:"row",justifyContent:"space-around"}}>
                      <TouchableOpacity onPress={this.openInfoPopUp.bind(this,item)}>
                      <Image 
                       source={require("../image/location.png")}style={{height:30,width:30}}/>
                      </TouchableOpacity>
                       <TouchableOpacity onPress={this.openCommentPopUp.bind(this,item,item.TestID)}>
                       <Image 
                       source={require("../image/comment.png")}style={{height:30,width:30}}/>
                       </TouchableOpacity>
                      </Right>  
                      
                      </View>
                    </View>       
                    </CardItem> }
                    </View>
                 )}
                 keyExtractor={(item, index) => index.toString()}
               />:null}
              </Card>:item.TestType == "PKG"?
                   <FlatList
                     data={item.OrderContentListInfo}
                     renderItem={({ item,index }) => (
                      <View>
                        {item.TestType=="GRP"?
                        <Card transparent style={styles.cardPatientInv}>
                         {this.packageViewEnable[index]==true?
                        <TouchableOpacity key={index}  activeOpacity={0.6} onLongPress={this.PackageCardLongPress.bind(this, index)}>
                        <CardItem header bordered 
                         style={styles.cardHeader}>
                           <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>
                           <Text style={{color:"#000",fontSize:14,fontWeight:"bold"}}>{item.TestName}</Text>
                              <Text style={{ color:"#bdb9b9",fontSize:13,paddingLeft:10,alignSelf:"stretch",textAlign:"center"}}>package Group</Text>
                           </Left>
                           <View>
                           <Right style={{ flexDirection:"row",justifyContent:"center"}} >
                           {this.packagePressCard[index]  ?
                            <CheckBox
                             checked={this.packageChecked[index]}
                             onPress={this.PackageCheckBoxHandleChange.bind(this,index,item)}
                            />: null}
                           <TouchableOpacity key={index} style={styles.row} onPress={this.packageExpanded.bind(this, index)}>
                           <Image source={this.packageExpandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                           style={{height:26,width:26,marginBottom:15}}/> 
                           </TouchableOpacity>
                           {this.packageViewEnable[index]==false || this.packageViewEnable==false ?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                          <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                          </Right>
                           </View>
                           
                         </CardItem>
                         </TouchableOpacity>:
                         <TouchableOpacity key={index}  activeOpacity={0.6}>
                         <CardItem header bordered 
                          style={styles.cardHeader}>
                            <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>
                            <Text style={{color:"#000",fontSize:14,fontWeight:"bold"}}>{item.TestName}</Text>
                              <Text style={{ color:"#bdb9b9",fontSize:13,paddingLeft:10,alignSelf:"stretch",textAlign:"center"}}>package Group</Text>
                            </Left>
                            <View>
                            <Right style={{ flexDirection:"row",justifyContent:"center"}} >
                            {this.packagePressCard[index]  ?
                            <CheckBox
                             checked={this.packageChecked[index]}
                             onPress={this.PackageCheckBoxHandleChange.bind(this,index,item)}
                            />: null}
                            <TouchableOpacity key={index} style={styles.row} onPress={this.packageExpanded.bind(this, index)}>
                            <Image source={this.packageExpandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                            style={{height:26,width:26,marginBottom:15}}/> 
                            </TouchableOpacity>
                            {this.packageViewEnable[index]==false || this.packageViewEnable==false ?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                          <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                           </Right>
                            </View>

                           
                          </CardItem>
                          </TouchableOpacity>
                         }
                         {this.packageExpandedBlock[index]  ?
                          <FlatList
                            data={item.ParameterListInfo}
                          renderItem={({ item,index }) => (
                            <View>
                              <CardItem style={styles.cardBody}>
                            <View style={{backgroundColor:"#fff", flex: 1,alignSelf:"center",width:350}}>
                              <View style={{flexDirection:"row",padding:10}}>
                              <Left>
                              <Text style={{margin:5}}>{item.TestName}</Text>
                              </Left>
                              <Right style={{padding:20}}>
                              <TouchableOpacity onPress={this.packageopenActionSheet.bind(this,item,index)}>
                                <Image 
                                source={require("../image/more.png")}style={{height:7,width:32}}/>
                              </TouchableOpacity> 
                              {this.pakgrpactioMENU[index]==true?
                              <MenuProvider style={{flex:1, flexDirection: 'column', padding: 60}}>
                                  <Menu
                                    opened={this.state.actionVisible}
                                    onBackdropPress={() => this.onBackdropPress()}>
                                    <MenuTrigger
                                      onPress={() => this.onTriggerPress()}>
                                  </MenuTrigger>
                                    <MenuOptions>
                                      <MenuOption onSelect={openRecollect} text='ReCollect' />
                                      <MenuOption onSelect={openRerun}text='ReRun' />
                                      <MenuOption onSelect={this.deltaDetails.bind(this, this.state.actionValue,this.state.patientName,this.state.visitNumber)} text='Delta' />
                                    </MenuOptions>
                                  </Menu>
                                </MenuProvider>:null}
                            </Right>
                            </View>
                            <View style={{flexDirection:"row",padding:10}}>
                            <Left>
                            <TouchableOpacity   onPress={this.openEditPopUp.bind(this,item.TestValue,item.TestID,item.ReferenceRange)}>
                            <Image 
                              source={require("../image/edit.png")}style={{height:20,width:20,margin:5}}/>
                            </TouchableOpacity>
                            {item.ReferenceColor=="LightGreen"?
                        <Text style={{fontWeight:"bold",color:"#42f5f2"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Red"?
                        <Text style={{fontWeight:"bold",color:"red"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="lightpink"?
                        <Text style={{fontWeight:"bold",color:"#ebabeb"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Yellow"?
                        <Text style={{fontWeight:"bold",color:"#e5ed6d"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Alert"?
                        <Text style={{fontWeight:"bold",color:"#d1d1d1"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Orange"?
                        <Text style={{fontWeight:"bold",color:"#f59e42"}}>{item.TestValue} {item.UOMCode}</Text>:
                        <Text style={{fontWeight:"bold"}}>{item.TestValue} {item.UOMCode}</Text>}
                            </Left>
                            <Right style={{flexDirection:"row",justifyContent:"space-around"}}>
                            <TouchableOpacity onPress={this.openInfoPopUp.bind(this,item)}>
                            <Image 
                              source={require("../image/location.png")}style={{height:30,width:30}}/>
                            </TouchableOpacity>
                              <TouchableOpacity onPress={this.openCommentPopUp.bind(this,item,item.TestID)}>
                              <Image 
                              source={require("../image/comment.png")}style={{height:30,width:30}}/>
                              </TouchableOpacity>
                            </Right>  
                            
                            </View>
                          </View>       
                          </CardItem> 
                            </View>
                          )}
                          keyExtractor={(item, index) => index.toString()}
                        />:null}
                    </Card>
                    :item.TestType=="INV" ?
                   <Card transparent style={styles.cardPatientInv}>
                    {this.packageViewEnable[index]==true?
                   <TouchableOpacity key={index}  activeOpacity={0.6} onLongPress={this.PackageCardLongPress.bind(this, index)}>
                    <CardItem header bordered 
                      style={styles.cardHeader}>
                        <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>
                        <Text style={{color:"#000",fontSize:14,fontWeight:"bold"}}>{item.TestName}</Text>
                        </Left>
                        <View>
                        <Right style={{ flexDirection:"row",justifyContent:"center"}} >
                        {this.packagePressCard[index]  ?
                        <CheckBox
                          checked={this.packageChecked[index]}
                          onPress={this.PackageCheckBoxHandleChange.bind(this,index,item)}
                            />: null}
                        <TouchableOpacity key={index} style={styles.row} onPress={this.packageExpanded.bind(this, index)}>
                       <Image source={this.packageExpandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                        style={{height:26,width:26,marginBottom:15}}/> 
                        </TouchableOpacity>
                        {this.packageViewEnable[index]==false || this.packageViewEnable==false ?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                        <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                      </Right>
                        </View>
                        
                      </CardItem>
                      </TouchableOpacity>:
                      <TouchableOpacity key={index}  activeOpacity={0.6}>
                      <CardItem header bordered 
                        style={styles.cardHeader}>
                          <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>                
                           <Text style={{color:"#000",fontSize:14,fontWeight:"bold"}}>{item.TestName}</Text>
                          </Left>
                          <View>
                          <Right style={{ flexDirection:"row",justifyContent:"center",alignSelf:"flex-end"}} >
                          {this.packagePressCard[index]  ?
                          <CheckBox
                            checked={this.packageChecked[index]}
                            onPress={this.PackageCheckBoxHandleChange.bind(this,index,item)}
                              />: null}
                          <TouchableOpacity key={index} style={styles.row} onPress={this.packageExpanded.bind(this, index)}>
                         <Image source={this.packageExpandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                          style={{height:26,width:26,marginBottom:15}}/> 
                          </TouchableOpacity>
                          {this.packageViewEnable[index]==false || this.packageViewEnable==false ?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                          <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                        </Right>
                          </View>
                          
                        </CardItem>
                        </TouchableOpacity>}
                      {this.packageExpandedBlock[index]  ?
                        <View>
                          <CardItem style={styles.cardBody}>
                          <View style={{backgroundColor:"#fff",alignSelf:"center",width:350}}>
                            <View style={{flexDirection:"row",padding:10}}>
                            <Left>
                            <TouchableOpacity  onPress={this.openEditPopUp.bind(this,item.TestValue,item.TestID,item.ReferenceRange)}>
                          <Image 
                            source={require("../image/edit.png")}style={{height:20,width:20,margin:5}}/>
                            </TouchableOpacity>
                            {item.ReferenceColor=="LightGreen"?
                        <Text style={{fontWeight:"bold",color:"#42f5f2"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Red"?
                        <Text style={{fontWeight:"bold",color:"red"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="lightpink"?
                        <Text style={{fontWeight:"bold",color:"#ebabeb"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Yellow"?
                        <Text style={{fontWeight:"bold",color:"#e5ed6d"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Alert"?
                        <Text style={{fontWeight:"bold",color:"#d1d1d1"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Orange"?
                        <Text style={{fontWeight:"bold",color:"#f59e42"}}>{item.TestValue} {item.UOMCode}</Text>:
                        <Text style={{fontWeight:"bold"}}>{item.TestValue} {item.UOMCode}</Text>}
                      
                          </Left>
                          <Right style={{padding:20}}>
                          <TouchableOpacity onPress={this.grpOpenActionSheet.bind(this,item,index)}>
                          <Image 
                            source={require("../image/more.png")}style={{height:7,width:32}}/>
                            </TouchableOpacity>
                            {this.grpactioMENU[index]==true?
                            <MenuProvider style={{flex:1, flexDirection: 'column', padding: 60}}>
                                <Menu
                                  opened={this.state.actionVisible}
                                  onBackdropPress={() => this.onBackdropPress()}>
                                  <MenuTrigger
                                    onPress={() => this.onTriggerPress()}>
                                </MenuTrigger>
                                  <MenuOptions>
                                    <MenuOption onSelect={openRecollect} text='ReCollect' />
                                    <MenuOption onSelect={openRerun}text='ReRun' />
                                    <MenuOption onSelect={this.deltaDetails.bind(this, this.state.actionValue,this.state.patientName,this.state.visitNumber)} text='Delta' />
                                  </MenuOptions>
                                </Menu>
                              </MenuProvider>:null}
                          </Right>
                          </View>
                          <View style={{flexDirection:"row",padding:10}}>
                          <Left>
                          </Left>
                          <Right style={{flexDirection:"row",justifyContent:"space-around"}}>
                          <TouchableOpacity onPress={this.openInfoPopUp.bind(this,item)}>
                          <Image 
                            source={require("../image/location.png")}style={{height:30,width:30}}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.openCommentPopUp.bind(this,item,item.TestID)}>
                          <Image 
                            source={require("../image/comment.png")}style={{height:30,width:30}}/>
                            </TouchableOpacity>
                          </Right>
                          </View>
                        </View>
                  </CardItem>
                  </View>:null}
                    </Card>:null
                    }
                  
                  </View>
              )}
              keyExtractor={(item, index) => index.toString()}
            />
                // Type:INV
              :item.TestType == "INV" ?
               <Card transparent style={styles.cardPatientInv}>
                 {this.viewEnable[index]==true?
              <TouchableOpacity key={index}  activeOpacity={0.6} onLongPress={this.cardLongPress.bind(this, index)}>
              <CardItem header bordered 
               style={styles.cardHeader}>
                 <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>
                 <Text style={{color:"#000",fontSize:14,fontWeight:"bold"}}>{item.TestName}</Text>
                 </Left>
                 <View>
                 <Right style={{ flexDirection:"row",justifyContent:"center"}} >
                 {this.pressCard[index]  ?
                 <CheckBox
                  checked={this.checked[index]}
                  onPress={this.checkBoxHandleChange.bind(this,index,item)}
                     />: null}
                 <TouchableOpacity key={index} style={styles.row} onPress={this.expanded.bind(this, index)}>
                   
                 <Image source={this.expandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                 style={{height:26,width:26,marginBottom:15}}/> 
                 </TouchableOpacity>
                 {this.viewEnable[index]==false || this.viewEnable==false?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                  <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                </Right>
                 </View>
                
               </CardItem>
               </TouchableOpacity>:
                 <TouchableOpacity key={index}  activeOpacity={0.6}>
                 <CardItem header bordered 
                  style={styles.cardHeader}>
                    <Left style={{flexDirection:"column",borderStartColor:"#7387eb",borderStartWidth:5}}>
                    <Text style={{color:"#000",fontSize:14,fontWeight:"bold"}}>{item.TestName}</Text>
                    </Left>
                    <View>
                    <Right style={{ flexDirection:"row",justifyContent:"center"}} >
                    {this.pressCard[index]  ?
                    <CheckBox
                    checked={this.checked[index]}
                    onPress={this.checkBoxHandleChange.bind(this,index,item)}
                        />: null}
                    <TouchableOpacity key={index} style={styles.row} onPress={this.expanded.bind(this, index)}>
                      
                    <Image source={this.expandedBlock[index] ? require("../image/expandUp.png"): require("../image/expandDown.png")}
                    style={{height:26,width:26,marginBottom:15}}/> 
                    </TouchableOpacity>
                    {this.viewEnable[index]==false || this.viewEnable==false?<Icon style={{padding:15,fontSize:25}} name="ios-eye"/>:
                    <Icon style={{padding:15,fontSize:25,color:"blue"}} name="ios-eye"/>}
                  </Right>
                    </View>
                    
                  </CardItem>
                  </TouchableOpacity>}
               {this.expandedBlock[index]  ?
                  <View>
                    <CardItem style={styles.cardBody}>
                   <View style={{backgroundColor:"#fff",alignSelf:"center",width:350}}>
                     <View style={{flexDirection:"row",padding:10}}>
                     <Left>
                     <TouchableOpacity  onPress={this.openEditPopUp.bind(this,item.TestValue,item.TestID,item.ReferenceRange)}>
                    <Image 
                     source={require("../image/edit.png")}style={{height:20,width:20,margin:5}}/>
                     </TouchableOpacity>
                     {item.ReferenceColor=="LightGreen"?
                        <Text style={{fontWeight:"bold",color:"#42f5f2"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Red"?
                        <Text style={{fontWeight:"bold",color:"red"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="lightpink"?
                        <Text style={{fontWeight:"bold",color:"#ebabeb"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Yellow"?
                        <Text style={{fontWeight:"bold",color:"#e5ed6d"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Alert"?
                        <Text style={{fontWeight:"bold",color:"#d1d1d1"}}>{item.TestValue} {item.UOMCode}</Text>:
                        item.ReferenceColor=="Orange"?
                        <Text style={{fontWeight:"bold",color:"#f59e42"}}>{item.TestValue} {item.UOMCode}</Text>:
                        <Text style={{fontWeight:"bold"}}>{item.TestValue} {item.UOMCode}</Text>}
                    </Left>
                    <Right style={{padding:20}}>
                  
                    <TouchableOpacity onPress={this.openActionSheet.bind(this,item,index)}>
                    <Image 
                     source={require("../image/more.png")}style={{height:7,width:32}}/>
                     </TouchableOpacity>
                      
                   {this.invactioMENU[index] == true?
                   <MenuProvider style={{flex:1, flexDirection: 'column', padding: 60}}>
                      <Menu
                        opened={this.state.actionVisible}
                        onBackdropPress={() => this.onBackdropPress()}>
                        <MenuTrigger
                          onPress={() => this.onTriggerPress()}>
                      </MenuTrigger>
                        <MenuOptions>
                          <MenuOption onSelect={openRecollect} text='ReCollect' />
                          <MenuOption onSelect={openRerun}text='ReRun' />
                          <MenuOption onSelect={this.deltaDetails.bind(this, this.state.actionValue,this.state.patientName,this.state.visitNumber)} text='Delta' />
                        </MenuOptions>
                      </Menu>
                    </MenuProvider>:null}
                    
                    </Right>
                    </View>
                    <View style={{flexDirection:"row",padding:10}}>
                    <Left>
                    </Left>
                    <Right style={{flexDirection:"row",justifyContent:"space-around"}}>
                    <TouchableOpacity onPress={this.openInfoPopUp.bind(this,item)}>
                    <Image 
                     source={require("../image/location.png")}style={{height:30,width:30}}/>
                     </TouchableOpacity>
                     <TouchableOpacity onPress={this.openCommentPopUp.bind(this,item,item.TestID)}>
                    <Image 
                     source={require("../image/comment.png")}style={{height:30,width:30}}/>
                     </TouchableOpacity>
                    </Right>
                    </View>
                  </View>
            </CardItem>
           </View>:null}
             </Card>:null}
           </View>
      
       )}
       keyExtractor={(item, index) => index.toString()}
     /> 
      {/* EditPopup */}
    <Dialog.Container contentStyle ={{width:350}}visible={this.state.editPopupvisible}>
        <View style={{flexDirection:"row",marginBottom:10}}>
          <Right>
         <Dialog.Title style={{fontWeight:"bold",fontSize:15,alignSelf:"flex-start"}}>Edit Test Value</Dialog.Title>
          </Right>
          <Left>
          <Icon onPress={handleCancel} style={{ color:"#000",alignSelf:"flex-end"}} name='close' />
          </Left>
        </View>
        <View style={{padding:5}}> 
       <TextInput
         ref={(input) => this.editValue = input}
         onChangeText={this.handleEditValue}
         defaultValue={this.state.editValue}
         style={styles.editValueInput}
     />
         <Text style={{fontWeight:"bold",fontSize:12}}>
            Reference Range
         </Text>
       <Text> {this.state.editReferenceRange} </Text>
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-around"}}>
        <TouchableOpacity  style={styles.discardBtn}  onPress={handleCancel}>
        <Text style={styles.discardBtnTxt}>
         Discard
        </Text>
        </TouchableOpacity>
        <TouchableOpacity  style={styles.updateBtn}  onPress={this.updateEditValue}>
         <Text style={styles.updateBtnTxt}>
          Update
         </Text>
        </TouchableOpacity>
       </View>
     </Dialog.Container>
     

 {/* Comment Popup */}

  <Dialog.Container contentStyle ={{width:350}} visible={this.state.commentPopupVisable}>
       <View style={{flexDirection:"row",marginBottom:10}}>
         <Left>
         <Icon onPress={handleCommentCancel} style={{ color:"#000",alignSelf:"flex-end"}} name='close' />
         </Left>
       </View>
       <View style={{padding:5}}> 
       <Card transparent style={styles.cardCommentPopup}>
        <CardItem header bordered 
         style={styles.cardHeader}>
         <Left>
             <Title style={{ color:"#000",fontSize:14,fontWeight:"bold",paddingLeft:10,alignSelf:"stretch"}}>Medical Comment</Title>
         </Left>
         <Right >
         <TouchableOpacity onPress={medicalexpanded} style={styles.row}>         
         <Image source={this.state.medicalExpand ? require("../image/expandUp.png"): require("../image/expandDown.png")}
         style={{height:26,width:26}}/> 
         </TouchableOpacity>
         </Right>
       </CardItem>
       {this.state.medicalExpand ?
        <CardItem style={styles.cardBody}>
           <TextInput
             ref={(input) => this.medicalComment = input}
             onChangeText={this.handleMedicalValue}
             placeholder={"Type your Medical Comment"}
             defaultValue={this.state.medicalComment}
             style={styles.medicalInput}
             multiline={true}
         />
            </CardItem>
           :null} 
        </Card>
        <Card transparent style={styles.cardCommentPopup}>
        <CardItem header bordered 
         style={styles.cardHeader}>
         <Left>
             <Title style={{ color:"#000",fontSize:14,fontWeight:"bold",paddingLeft:10,alignSelf:"stretch"}}>Technical Comment</Title>
         </Left>
         <Right>
         <TouchableOpacity onPress={techincalexpanded} style={styles.row}>         
         <Image source={this.state.techincalExpand ? require("../image/expandUp.png"): require("../image/expandDown.png")}
         style={{height:26,width:26}}/> 
         </TouchableOpacity>
         </Right>
       </CardItem>
       {this.state.techincalExpand ?
        <CardItem style={styles.cardBody}>
           <TextInput
             ref={(input) => this.technicalComment = input}
             onChangeText={this.handleTechnicalValue}
             placeholder={"Type your Technical Comment"}
             defaultValue={this.state.technicalComment}
             style={styles.medicalInput}
             multiline={true}
         />
            </CardItem>:null}
        </Card>
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-around"}}>
        <TouchableOpacity  style={styles.discardBtn}  onPress={handleCommentCancel}>
        <Text style={styles.discardBtnTxt}>
         Discard
        </Text>
        </TouchableOpacity>
        <TouchableOpacity  style={styles.updateBtn}  onPress={handleAddComment}>
         <Text style={styles.updateBtnTxt}>
          Add Comment
         </Text>
        </TouchableOpacity>
       </View>
     </Dialog.Container>

       {/* Information Popup */}
       <Dialog.Container contentStyle ={{width:350}} visible={this.state.infoPopupVisable}>
       <View style={{flexDirection:"row",marginBottom:10}}>
         <Left>
         <Icon onPress={handleInfoCancel} style={{ color:"#000",alignSelf:"flex-end"}} name='close' />
         </Left>
       </View>
       <View style={{padding:5}}> 
       <Card transparent style={styles.cardCommentPopup}>
        <CardItem header bordered 
         style={styles.cardHeader}>
         <Left>
             <Title style={{ color:"#000",fontSize:14,fontWeight:"bold",paddingLeft:10,alignSelf:"stretch"}}>Reference Range</Title>
         </Left>
       </CardItem>
        <CardItem style={styles.cardBody}>
         <Text>{this.state.infoPopupValue.ReferenceRange}</Text>
            </CardItem>
            </Card>
            <Card>
            <CardItem header bordered 
         style={styles.cardHeader}>
         <Left>
             <Title style={{ color:"#000",fontSize:14,fontWeight:"bold",paddingLeft:10,alignSelf:"stretch"}}>Device Value</Title>
         </Left>
       </CardItem>
        <CardItem style={styles.cardBody}>
         <Text>{this.state.infoPopupValue.DeviceValue}</Text>
         <Text>Device Value Details Here...</Text>
            </CardItem>
        </Card>
        <Card>
            <CardItem header bordered 
         style={styles.cardHeader}>
         <Left>
             <Title style={{ color:"#000",fontSize:14,fontWeight:"bold",paddingLeft:10,alignSelf:"stretch"}}>QC Value</Title>
         </Left>
       </CardItem>
        <CardItem style={styles.cardBody}>
         <Text>Qc Value Details Here...</Text>
            </CardItem>
        </Card>
        </View>
       </Dialog.Container>

   {/* Action Model Popup */}
      {/* <Dialog.Container  
         visible = {this.state.actionVisible}
         onBackdropPress={closeActionSheet}
       > 
         <Dialog.Title onPress={openRecollect} >Recollect</Dialog.Title>
         <Dialog.Title onPress={openRerun}>Rerun</Dialog.Title>
         <Dialog.Title  onPress={this.deltaDetails.bind(this, this.state.actionValue)}>Delta</Dialog.Title>
       </Dialog.Container> */}

       
       <Dialog.Container contentStyle ={{width:350}} visible={this.state.recollectVisable}>
       <View style={{flexDirection:"row",marginBottom:10}}>
         <Right>
         <Dialog.Title style={{fontWeight:"bold",fontSize:15,alignSelf:"flex-start"}}>Do you want the re-collect sample?</Dialog.Title>
         </Right>
         <Left>
         <Icon onPress={closeReCollect} style={{ color:"#000",alignSelf:"flex-end"}} name='close' />
         </Left>
       </View>
       <View style={{padding:5}}> 
       {this.state.recollectReson!=null || this.state.recollectReson!="" ?
      <DropDownPicker
        items={reasonDropDownitems}
        containerStyle={{height: 40,width:310}}
        placeholder={this.state.recollectReson}
        style={{backgroundColor: '#fafafa'}}
        //defaultValue={this.state.recollectReson}
        itemStyle={{
            justifyContent: 'flex-start'
        }}
        dropDownStyle={{backgroundColor: '#fafafa',height: 180}}
        onChangeItem={this.handlereCollectValue}
       />:
       <DropDownPicker
       items={reasonDropDownitems}
       containerStyle={{height: 40,width:310}}
       placeholder="Select Re-Collect Reason"
       style={{backgroundColor: '#fafafa'}}
       //defaultValue={this.state.recollectReson}
       itemStyle={{
           justifyContent: 'flex-start'
       }}
       dropDownStyle={{backgroundColor: '#fafafa',height: 180}}
       onChangeItem={this.handlereCollectValue}
      />}
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-around",marginTop:150}}>
        <TouchableOpacity  style={styles.discardBtn}  onPress={closeReCollect}>
        <Text style={styles.discardBtnTxt}>
         Cancel
        </Text>
        </TouchableOpacity>
        {this.state.recollectReson!=null || this.state.recollectReson!="" ?
        <TouchableOpacity  style={styles.updateBtn}  onPress={UpdateRecollect}>
         <Text style={styles.updateBtnTxt}>
          Re-Recollect
         </Text>
        </TouchableOpacity>:
        <TouchableOpacity disabled style={styles.updateBtn}>
        <Text style={styles.updateBtnTxt}>
         Re-Recollect
        </Text>
       </TouchableOpacity>}
       </View>
     </Dialog.Container>
     <Dialog.Container contentStyle ={{width:350}} visible={this.state.rerunVisable}>
       <View style={{flexDirection:"row",marginBottom:10}}>
         <Right>
         <Dialog.Title style={{fontWeight:"bold",fontSize:15,alignSelf:"flex-start"}}>Do you want the re-run test?</Dialog.Title>
         </Right>
         <Left>
         <Icon onPress={closeReRun} style={{ color:"#000",alignSelf:"flex-end"}} name='close' />
         </Left>
       </View>
       <View style={{padding:5}}> 
       {this.state.rerunReson!=null || this.state.rerunReson!=""?
       <DropDownPicker
        items={reasonDropDownitems}
        containerStyle={{height: 40,width:310}}
        placeholder={this.state.rerunReson}
        style={{backgroundColor: '#fafafa'}}
        //defaultValue={this.state.recollectReson}
        itemStyle={{
            justifyContent: 'flex-start'
        }}
        dropDownStyle={{backgroundColor: '#fafafa',height: 180}}
        onChangeItem={this.handlereRunValue}
       />:
       <DropDownPicker
       items={reasonDropDownitems}
       containerStyle={{height: 40,width:310}}
       placeholder="Select Re-Run Reason"
       style={{backgroundColor: '#fafafa'}}
       //defaultValue={this.state.recollectReson}
       itemStyle={{
           justifyContent: 'flex-start'
       }}
       dropDownStyle={{backgroundColor: '#fafafa',height: 180}}
       onChangeItem={this.handlereRunValue}
      />}
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-around",marginTop:150}}>
        <TouchableOpacity  style={styles.discardBtn}  onPress={closeReRun}>
        <Text style={styles.discardBtnTxt}>
         Cancel
        </Text>
        </TouchableOpacity>
        {this.state.rerunReson !=null || this.state.rerunReson !="" ?
        <TouchableOpacity  style={styles.updateBtn}  onPress={UpdateRun}>
         <Text style={styles.updateBtnTxt}>
          Re-Run
         </Text>
        </TouchableOpacity>:
        <TouchableOpacity  disabled style={styles.updateBtn}>
        <Text style={styles.updateBtnTxt}>
         Re-Run
        </Text>
       </TouchableOpacity>}
       </View>
     </Dialog.Container>
     </Content>
       <Footer style={{backgroundColor:"#fff",justifyContent:"space-around"}}>
       {this.allCheckClick==false && this.state.viewEye == true? 
       <TouchableOpacity onPress={this.submitApproveAll} style={styles.approveBtn}>
            <Text style={styles.approveBtnTxt}>Approve</Text>
            </TouchableOpacity>:
           <TouchableOpacity disabled style={styles.approveBtnDisable}>
           <Text style={styles.approveBtnTxt}>Approve</Text>
           </TouchableOpacity>}
           {this.allCheckClick==true ?
           <TouchableOpacity onPress={this.submitApproveAll} style={styles.approveAllBtn} >
           <Text style={styles.approveAllBtnTxt}>Approve all</Text>
           </TouchableOpacity>:
            <TouchableOpacity disabled style={styles.approveAllBtnDisable} >
            <Text style={styles.approveAllBtnTxt}>Approve all</Text>
            </TouchableOpacity>}
       </Footer>
      </Container>
      </SafeAreaView>
    );
  }
}
export default InvestigationDetails;
const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingTop:10,
    backgroundColor:"#FFF",
    
   },
   editValueInput: {
    width:  wp("80%"),
    height: 48,
    padding: 10,
    borderWidth: 2,
    borderRadius:5,
    borderBottomColor:'#eee',
    borderRightColor:'#eee',
    borderLeftColor:'#eee',
    borderTopColor:'#eee',
    textAlign:"left",
    marginBottom: 20,
    alignSelf:"center",
  },
  medicalInput:{
    width:  wp("70%"),
    height: 48,
    padding: 10,
    borderWidth: 2,
    borderRadius:5,
    borderBottomColor:'#fff',
    borderRightColor:'#fff',
    borderLeftColor:'#fff',
    borderTopColor:'#fff',
    textAlign:"left",
    marginBottom: 20,
    alignSelf:"center",
    backgroundColor:"#fff"
  },
  discardBtn:{
    backgroundColor:"#eee",
    width:  wp("30%"),
    borderRadius: 10,
    marginVertical: 10,
    paddingVertical: 13,
    alignSelf:"center"
  },
  discardBtnTxt:{
    fontSize: 16,
    fontWeight: '500',
    color: '#ed66aa',
    textAlign: 'center',
  },
  updateBtn:{
    backgroundColor:"#ed66aa",
    width:  wp("30%"),
    borderRadius: 10,
    marginVertical: 10,
    paddingVertical: 13,
    alignSelf:"center"
  },
  updateBtnTxt:{
    fontSize: 16,
    fontWeight: '500',
    color: '#FFF',
    textAlign: 'center',
  },
  approveBtnDisable:{
    backgroundColor:"#b4d9ed",
    width:  wp("40%"),
    borderRadius: 10,
    marginVertical: 5,
    paddingVertical: 13,
    alignSelf:"center"
  },
  approveBtn:{
    backgroundColor:"#7387eb",
    width:  wp("40%"),
    borderRadius: 10,
    marginVertical: 5,
    paddingVertical: 13,
    alignSelf:"center"
  },
  approveBtnTxt:{
    fontSize: 15,
    fontWeight: "bold",
    color: '#fff',
    textAlign: 'center',
  },
  approveAllBtnDisable:{
    backgroundColor:"#e697b9",
    width:  wp("40%"),
    borderRadius: 10,
    marginVertical: 5,
    paddingVertical: 13,
    alignSelf:"center"
  },
  approveAllBtn:{
    backgroundColor:"#ed66aa",
    width:  wp("40%"),
    borderRadius: 10,
    marginVertical: 5,
    paddingVertical: 13,
    alignSelf:"center"
  },
  approveAllBtnTxt:{
    fontSize: 15,
    fontWeight: "bold",
    color: '#FFF',
    textAlign: 'center',
  },
  visitId:{ 
      color: '#fff',
      fontSize:12
    },
    cardPatientInv:{ 
      alignSelf:"center",
      width: wp("95%"),
      backgroundColor:"#eee"
  },
  cardHeader:{
    backgroundColor: "#eee",
    alignSelf:"flex-start",
    borderBottomColor:"#fff",
    borderBottomWidth:2,
  },
  cardBody:{
    backgroundColor: "#eee",
  },
  cardCommentPopup:{
    alignSelf:"center",
    width: wp("80%"),
    backgroundColor:"#eee"
  },
});