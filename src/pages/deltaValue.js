import React, { Component } from 'react';
import { View,Text, StyleSheet ,SafeAreaView,FlatList,Image,TouchableOpacity,BackHandler, Modal,TextInput} from 'react-native';
import { Container, Header,Card, Content,CardItem,checked, Left,Toast, Footer,ActionSheet, FooterTab,Body, Right, Button, Icon,Subtitle, Title,Tab, Tabs,ScrollableTab } from 'native-base';
import { Actions } from "react-native-router-flux";
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as url from "../url";
import moment from 'moment';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
class DeltaValue extends Component {

    constructor(props) {
        super(props);
        this.state = {
            deltaDetails:props.data,
            patientName:props.patientName,
            visitNumber:props.visitNumber,
            deltaValues:[],
        }
    }
    componentDidMount() {
      console.log(this.state.deltaDetails,"-->delta");
      console.log(this.state.patientName,"-->patientName");
      console.log(this.state.visitNumber,"-->visitNumber");
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            Actions.pop();
            return true;
        });
        this.getDeltaValues();
    }
    componentWillUnmount() {
        //  this.backHandler.remove();
       }
//Service
base_url = url.base_url;
getDeltaValues= async () =>{
  const URL=await AsyncStorage.getItem('instanceURL');
  const sessionId=await AsyncStorage.getItem('SessionID');
  const loginId=await AsyncStorage.getItem('LoginID');
  const orgId=await AsyncStorage.getItem('orgId');
  const patientVisitID=this.state.deltaDetails.PatientVisitID;
  const patternID=this.state.deltaDetails.PatternID
  const InvID=this.state.deltaDetails.TestID
 fetch(
    URL + "API/InvestigationValues/GetDeltaValues?OrgID="+orgId+"&patientVisitID="+patientVisitID+"&PatternID="+patternID+"&InvID="+InvID+"",
    { 
   method: 'GET',
     headers: {
       'Content-Type': 'application/json',
       "ResWrapperReqYN":"Y",
       "LoginID":loginId,
       "SessionID":sessionId
       }
       }).then((response) => response.json())
         .then((responseJson) => { 
           console.log(responseJson,"-->responseJson");
         if (responseJson.status =="OK") {
           this.setState({ deltaValues: responseJson.response});
         } else {
           this.sessionExpired(responseJson);
         }
         })
         .catch((error) => {
           console.error(error);
         });
 }

 showToast = (message) => {
   Toast.show({
     text: message,
     buttonText: "Okay",
     duration: 3000
   })
 }

 sessionExpired = (responseJson) => {
   if (responseJson.status == "InternalServerError" ||
   responseJson.Message == "An error has occurred.") {
       // AsyncStorage.clear();
       // Actions.login();
       if (responseJson.status == "InternalServerError")
        {
         this.showToast(responseJson.status);
       }else if(responseJson.Message == "An error has occurred."){
         // 
         console.log("An error has occurred.");
       }
       
     
   }
} 


//Design

    render() {
          return (
            <SafeAreaView  style={{flex:1}}>
           <Container>
            <Header style={{ backgroundColor:"#7387eb"}}>
           <Left>
           <Icon onPress={() => Actions.pop()} style={{ color:"#fff"}} name="arrow-back"/>
           </Left>
           <View>
           <Body>
           {Platform.OS === 'android'?
           <Title style={{ fontSize:15}}>{this.state.patientName}</Title>:
            Platform.OS === 'ios'?
            <Title style={{ fontSize:15,color:"#FFF"}}>{this.state.patientName}</Title>:null}
            <Subtitle style={styles.visitId}>{this.state.visitNumber}</Subtitle>
           </Body>
           </View>
           
           <Right >
           <Icon onPress={() => Actions.dashboard()} 
               style={{ color:"#fff"}} 
               name="home-outline"
           />
          </Right>
           </Header> 
            <Content>
            <View style={styles.InvName}>
              <View>
              <Text>History of result for</Text> 
              </View>
              <View>
              <Text style={{fontWeight:"bold",margin:5}} >{this.state.deltaDetails.TestName}</Text>  
              </View>
           </View>
         <View style={{ padding:15}}>
          {this.state.deltaValues.length == 0 ?
             <View style={styles.noDataFound}>
               <Text style={{ color: 'grey' }}>No Records Found</Text>
               </View> :
              
            <FlatList
             data={this.state.deltaValues}
             renderItem={({ item }) => ( 
        
                <Card transparent style={styles.cardPatient} >
                <CardItem header style={styles.cardHeader}>
                <Left>
                    <Text style={{ fontWeight:"bold",margin:6}}>{item.VisitID}</Text>
                    </Left>
                  <Right>
                    <Text>{moment(item.CreatedAt).format('DD MMM YYYY hh:mm A')}</Text>
                  </Right>
                </CardItem>
                  <CardItem  style={styles.cardBody}>
                      <View >
                      <Text  style={{ fontWeight:"bold",margin:5}}>{item.Value} mg/dl</Text>
                      <View style={{ padding:5}}>
                       <Text  style={{ fontWeight:"bold",fontSize:13}}> ReferenceRange</Text>
                       <Text  style={{fontSize:13}}> {item.ReferenceRange}</Text>
                      </View>
                      </View>
                   
                </CardItem>
              </Card>
            
               )}
               keyExtractor={(item, index) => index.toString()}
             />}
         </View>
        </Content>
           </Container>
            </SafeAreaView>
           
           
          );
    }
}
export default DeltaValue;
const styles = StyleSheet.create({
    visitId:{ 
        color: '#fff',
        fontSize:12
      },
      container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
      },
      cardPatient:{ 
        alignSelf:"center",
        width: wp("95%"),
        backgroundColor:"#eee"
    },
    cardHeader:{
      backgroundColor: "#eee",
      borderBottomColor:"#fff",
      borderBottomWidth:3
    },
    cardBody:{
      backgroundColor: "#eee",
    },
    cardFooter:{
      backgroundColor: "#eee",
    },
    noDataFound: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    InvName:{
      padding:10,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:"#ccc",
      flexDirection:"row"
      
    }
});