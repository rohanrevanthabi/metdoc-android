import React, { Component } from 'react';
import { StyleSheet, View,BackHandler,TouchableOpacity,Text } from 'react-native';
import { Container, Header,Card, Content,CardItem, Left, Footer, FooterTab,Body, Right, Button, Icon,Subtitle, Title,Tab, Tabs,ScrollableTab } from 'native-base';
import { Actions } from "react-native-router-flux";
import { WebView } from 'react-native-webview';
import * as url from "../url";
class DownloadPdfReports extends Component {

    constructor(props) {
        super(props);
        this.state = {
            // isLoading: true,
             data: props.data,
             url:props.url,
            downloadIcon:false
        };
    }
    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            Actions.pop();
            return true;
        });
    }
    componentWillUnmount() {
     
    }
    activeDownloadIcon() {
        this.setState({ downloadIcon: true });
       }
  render() {
    if (this.state.isLoading) {
        return (
            //loading view while data is loading
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator size='large' color='#1E88E5' />
            </View>
        );
    }
    const source = this.state.url+"API/InvestigationValues/ConvertFilePathToPdf?OrgID="+this.state.data.OrgID+"&VisitID="+this.state.data.PatientVisitId+"&OrgAddressID="+this.state.data.OrgAddressID+"&ReportType&Stationary=true"
    // const source = "http://google.com"
    console.log(source);
    return (
      <Container>
        <Header style={{ backgroundColor:"#fff"}}>
        <Left>
         <Icon onPress={() => Actions.pop()} style={{ color:"#000"}} name="arrow-back"/>
        </Left>
         <Body>
           <Title style={{ fontSize:15,color:"#000"}}>Download Pdf</Title>
        </Body>
        <Right >
         <Icon 
          onPress={() => Actions.dashboard()} 
          style={{ color:"#ccc"}} 
          name="home-outline"
        />
        </Right>
       </Header>
        <WebView 
          source={{ uri: source }} 
        />
        <View style={styles.container}>
        <Text style={{ fontSize:20,color:"#000"}}>Downloaded Pdf</Text>
        </View>
      </Container>

    );
  }
}
export default DownloadPdfReports;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent:"center",
    alignItems:"center"
  },
});