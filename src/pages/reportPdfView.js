import React, { Component } from 'react';
import { StyleSheet, View,BackHandler,TouchableOpacity } from 'react-native';
import { Container, Header,Card, Content,CardItem, Left, Footer, FooterTab,Body, Right, Button, Icon,Subtitle, Title,Tab, Tabs,ScrollableTab } from 'native-base';
//  import PDFReader from 'rn-pdf-reader-js';
import PDFView from 'react-native-view-pdf';
import { Actions } from "react-native-router-flux";
import * as url from "../url";
class ReportPdfView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            // isLoading: true,
             data: props.data,
             url:props.url,
            downloadIcon:false
        };
        this.pdf = null;
    }
    componentDidMount() {
      console.log(this.state.data,"-->data");
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            Actions.pop();
            return true;
        });
    }
    componentWillUnmount() {
     
    }
    activeDownloadIcon() {
        this.setState({ downloadIcon: true });
       }
  render(){
    if (this.state.isLoading) {
        return (
            //loading view while data is loading
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator size='large' color='#1E88E5' />
            </View>
        );
    }
    const source = this.state.url+"API/InvestigationValues/ConvertFilePathToPdf?OrgID="+this.state.data.OrgID+"&VisitID="+this.state.data.PatientVisitId+"&OrgAddressID="+this.state.data.OrgAddressID+"&ReportType&Stationary=true"
    const resources = {
      file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
      url: source,
      // base64: 'JVBERi0xLjMKJcfs...',
    };
    const resourceType = 'url';
    
    return (
      <Container>
        <Header style={{ backgroundColor:"#fff"}}>
        <Left>
         <Icon onPress={() => Actions.pop()} style={{ color:"#000"}} name="arrow-back"/>
        </Left>
         <Body>
           <Title style={{ fontSize:15,color:"#000"}}>Reports Pdf</Title>
        </Body>
        <Right >
         <Icon 
          onPress={() => Actions.dashboard()} 
          style={{ color:"#ccc"}} 
          name="home-outline"
        />
        </Right>
       </Header>
        {/* <View style={styles.container}>
         <PDFReader
          ref={(pdf)=>{this.pdf = pdf;}}
          withScroll={true}
          withPinchZoom={true}
          onError={(error)=>{console.log(error);}} 
          source={{ uri: source }}
        />
        </View> */}
        <View style={{ flex: 1 }}>
        {/* Some Controls to change PDF resource */}
        <PDFView
          fadeInDuration={250.0}
          style={{ flex: 1 }}
          resource={resources[resourceType]}
          resourceType={resourceType}
          onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
          onError={(error) => console.log('Cannot render PDF', error)}
        />
      </View>
      </Container>

    );
  }
}
export default ReportPdfView;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
});